$("#liproductos").on('show.bs.collapse', function(){
    $("#referencia").collapse('hide');
    $('#navcontend').animate({scrollTop: ($('#referencia').offset().top)}, 1500);
});
$("#referencia").on('show.bs.collapse', function(){
    $("#liproductos").collapse('hide');
    $('#navcontend').animate({scrollTop: ($('#referencia').offset().top)}, 1500);
});
$(".submenu").hover(function(){
    $(".submenu").removeClass('active');
    $(this).addClass('active');
    if(!$($(this).attr('href')).hasClass('show')){
        $(".collapse").collapse('hide');
        $($(this).attr('href')).collapse('show');
    }
},function(){});
$(".nosubmenu").hover(function(){
    $(".collapse").collapse('hide');
    $(".submenu").removeClass('active');
},function(){});
$(".submenu").on('click',function(event){
    event.preventDefault();
});
$('.toggle-button').on('click', function() {
    $(this).toggleClass('button-open');
    if($(this).hasClass('button-open')){
        $('#navdelsay>div:nth-child(2)').addClass('open');
        if($('#acd').hasClass('active'))
            $('#acd').removeClass('active');
        if($( window ).width() > 575){
            var widthnav = "0";
            if($( window ).width() > 575 && $( window ).width() < 768)
                widthnav = "80%";
            if($( window ).width() > 767 && $( window ).width() < 992)
                widthnav = "70%";
            if($( window ).width() > 991 && $( window ).width() < 1399)
                widthnav = "60%";
            if($( window ).width() > 1399)
                widthnav = "50%";        
            setTimeout(function(){
                $('#navdelsay').animate({'width':widthnav},500);
            },400);
        }else{
            setTimeout(function(){
                $('#navdelsay').animate({'height':'100%'},500);
            },400)
        }
    }else{
        $("#navdelsay .collapse").collapse('hide');
        $('#navdelsay>div:nth-child(2)').removeClass('open');
        if($( window ).width() > 575){
            setTimeout(function(){
                $('#navdelsay').animate({'width':'40px'},500);
            },400);
        }else{
            setTimeout(function(){
                $('#navdelsay').animate({'height':'40px'},500);
            },400)
        }
    }
});
$( window ).on('resize',function(event) {
    if($( window ).width() > 575){
        $('#navdelsay').animate({'width':'40px','height':'100%'},500);
        $('#navdelsay>div:nth-child(2)').removeClass('open');
        $('.toggle-button').removeClass('button-open');
    }else{
        $('#navdelsay').animate({'width':'100%','height':'40px'},500);
        $('#navdelsay>div:nth-child(2)').removeClass('open');
        $('.toggle-button').removeClass('button-open');
    }
});
/*
$(document).on('click', '.animatescroll', function (event) {
	event.preventDefault();
	$('html, body').animate({scrollTop: ($($.attr(this, 'href')).offset().top) - 110}, 1500);
});
*/
/**/