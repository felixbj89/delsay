var prinbanner = new Swiper('#prinbanner', {
    preloadImages: false,
    lazy: true,    
    spaceBetween: 40,
    centeredSlides: true,
    speed: 700,
    autoplay: {
        delay: 9000,
        disableOnInteraction: false,
    },
    resizeReInit: true,
    pagination: {
        el: '#prinbanner .swiper-pagination',
        clickable: true,
    },
});
prinbanner.on('slideChange', function () {
    $('.eslogan').velocity({translateX: '100%' }, { display:"none",duration:1000 });
    setTimeout(function(){
        $('#eslogan_'+prinbanner.activeIndex).velocity({translateX: ['0%','100%'] }, { display:"block",duration:1000 });
    },1000);
});
var productosswiper =  new Swiper('#productosswiper', {
    slidesPerView: 2,
    slidesPerColumn: 2,
    spaceBetween: 0,
    keyboard: {
        enabled: true,
    },
    pagination: {
        el: '#productosswiper .swiper-pagination',
        clickable: true,
    },
    resizeReInit: true,
    breakpoints: {
        575: {
            slidesPerView: 1,
            slidesPerColumn: 2
        }
    }
});
productosswiper.on('slideChange', function () {
    setTimeout(function(){
        $("#productosswiper .swiper-slide-active div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    },50);
});
var popularesswiper =  new Swiper('#popularesswiper', {
    slidesPerView: 2,
    slidesPerColumn: 2,
    spaceBetween: 0,
    keyboard: {
        enabled: true,
    },
    pagination: {
        el: '#popularesswiper .swiper-pagination',
        clickable: true,
    },
    resizeReInit: true,
    breakpoints: {
        991: {
            slidesPerView: 1,
            slidesPerColumn:2
        },
        575: {
            slidesPerView: 1,
            slidesPerColumn: 1
        },
    }
});
popularesswiper.on('slideChange', function () {
    setTimeout(function(){
        $("#popularesswiper .swiper-slide-active div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    },50);
});
var tiendasswiper =  new Swiper('#tiendasswiper', {
    slidesPerView: 2,
    slidesPerColumn: 3,
    spaceBetween: 0,
    slidesPerColumnFill: 'column',
    keyboard: {
        enabled: true,
    },
    pagination: {
        el: '#tiendasswiper .swiper-pagination',
        clickable: true,
    },
    resizeReInit: true,
    breakpoints: {
        1299: {
            slidesPerView: 2,
            slidesPerColumn: 2
        },
        991: {
            slidesPerView: 1,
            slidesPerColumn: 2
        },
        /*575: {
            slidesPerView: 1,
            slidesPerColumn: 3
        },*/
    }
});
tiendasswiper.on('slideChange', function () {
    setTimeout(function(){
        $("#tiendasswiper .swiper-slide div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    },50);
});
var noticiasswiper =  new Swiper('#noticiasswiper', {
    slidesPerView: 4,
    slidesPerColumn: 3,
    spaceBetween: 0,
    slidesPerColumnFill: 'row',
    keyboard: {
        enabled: true,
    },
    pagination: {
        el: '#noticiasswiper .swiper-pagination',
        clickable: true,
    },
    resizeReInit: true,
    breakpoints: {
        1199:{
            slidesPerView: 3,
            slidesPerColumn: 2
        },
        767: {
            slidesPerColumnFill: 'column',
            slidesPerView: 1,
            slidesPerColumn: 2
        }
    }
});
noticiasswiper.on('slideChange', function () {
    setTimeout(function(){
        $("#noticiasswiper .swiper-slide-active div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    },50);
});

var swiperdelsay = new Swiper('#swiperdelsay', {
    direction: 'vertical',
    slidesPerView: 1,
    spaceBetween: 0,
    speed: 500,
    mousewheel: true,
    slideToClickedSlide: false,
});
swiperdelsay.on('slideChange', function () {
    setTimeout(function(){
        $("#swiperdelsay .swiper-slide-active div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 3000
        });
    },10);
    $('#navcontend > div.row > ul[class*="col"] > li > a').removeClass('active');
    $('#navcontend > div.row > ul[class*="col"] > li > a[data-swiper="'+swiperdelsay.activeIndex+'"]').addClass('active');
});

var redesswiper =  new Swiper('#redesswiper', {
    slidesPerView: 5,
    slidesPerColumn: 2,
    spaceBetween: 0,
    keyboard: {
        enabled: true,
    },
    pagination: {
        el: '#redesswiper .swiper-pagination',
        clickable: true,
    },
    resizeReInit: true,
    breakpoints: {
        1299: {
            slidesPerView: 5,
            slidesPerColumn: 2
        },
        991: {
            slidesPerView: 3,
            slidesPerColumn: 2
        },        
        575: {
            slidesPerView: 1,
            slidesPerColumn: 2
        }
    }
});
redesswiper.on('slideChange', function () {
    setTimeout(function(){
        $("#redesswiper .swiper-slide-active.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    },50);
});

$(window).bind("resize", function() {
    prinbanner.update();
    productosswiper.update();
    popularesswiper.update();
    tiendasswiper.update();
    noticiasswiper.update();
    redesswiper.update();
});
/*window.onresize = function(event) {};*/
/**/
$("div.lazyload").lazyload({
    effect: "fadeIn",
    effectTime: 300,
    threshold: 2000
});
$(".blureffect").hover(function(){
    var yo = $(this);
    $(yo.data('blur')).addClass('blur_effect');
    yo.find('.isotipo').fadeOut( "fast" );
    yo.find('.tostore').fadeIn( "fast" );
},function(){
    var yo = $(this);
    $(yo.data('blur')).removeClass('blur_effect');
    yo.find('.tostore').fadeOut( "fast" );
    yo.find('.isotipo').fadeIn( "fast" );
});
/*function menustate(id) {
    if(id != ''){
        $('.slideTo').removeClass('active');
        $(id).addClass('active');
    }else{
        $('.slideTo').removeClass('active');
    }
}*/
/*var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: 'onLeave'}});
controller.scrollTo(function(target) {
    TweenMax.to(window, 0.5, {
        scrollTo : {
        y : target, // scroll position of the target along y axis
        autoKill : true // allows user to kill scroll action smoothly
        },
        ease : Cubic.easeInOut,
        duration: '300'
    });
});*/
$(document).ready(function(){
    /*new ScrollMagic.Scene({triggerElement: "#banner"})
        .setPin("#banner")
        .addTo(controller);    
    new ScrollMagic.Scene({triggerElement: "#productos"})
        .setPin("#productos")
        .on("enter", function (event) {menustate('#hight1');})
        .on("leave", function(e) {menustate('');})
        .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#populares"})
        .setPin("#populares")
        .on("enter", function (event) {menustate('#hight2');})
        .on("leave", function(e) {menustate('#hight1');})
        .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#noticias"})
        .setPin("#noticias")
        .on("enter", function (event) {menustate('#hight3');})
        .on("leave", function(e) {menustate('#hight2');})
        .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#tiendas"})
        .setPin("#tiendas")
        .on("enter", function (event) {menustate('#hight4');})
        .on("leave", function(e) {menustate('#hight3');})
        .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#contacto"})
        .setPin("#contacto")
        .on("enter", function (event) {menustate('#hight5');})
        .on("leave", function(e) {menustate('#hight4');})
        .addTo(controller);*/
    /**cargar-noticia**/
    //.substr(0,window.location.href.search("public"))
    var url = window.location.href;
    var route ="datanews/news.json";
    var labels = "";

    setTimeout(function(){
        $('#eslogan_0').velocity({translateX: ['0%','100%'] }, { display:"block",duration:1000 });
    },1000);

    $.getJSON( url + route, function( news ) {
        $.each( news, function( key, val ) {
            localforage.setItem(key,val);
            $.each( val.labels.split('/'), function( i,label ) {
                labels += '<h5 class="d-inline-block mb-1 mr-1"><span class="delsaytag">'+label.toUpperCase()+'</span></h5>';
            });
            $("#"+key+">div[class*='col']:nth-child(1)>div").html(labels);
            $("#"+key+">div[class*='col']:nth-child(2)>p").text(val.littletitle.toUpperCase());
            $("#"+key+">div[class*='col']:nth-child(3)>h5").text(val.bigtitle.toUpperCase());
            $("#"+key+">div[class*='col']:nth-child(4)>p").text(val.date.toUpperCase());
            labels = "";
        });
    });
    /***/
    $.mask.definitions['a'] = "[1-9]";
    $("#telefono,#telefonoI").mask("+58 a99 999 9999",{autoclear: false,placeholder:""});
    $('#formcontacto input,#formcontacto textarea,input[type=checkbox]').on('keyup blur click', function (){
        if ($('#nombre').hasClass('valid') && $('#email').hasClass('valid') && $('#telefono').hasClass('valid') && $('#asunto').hasClass('valid') && $('#mensaje').hasClass('valid') && $('#formcontacto input[type=checkbox]:checked').length > 0){
            $('#formcontacto #su_contact').prop('disabled', false);
        } else {
            $('#formcontacto #su_contact').prop('disabled', 'disabled');
        }
    });
    $('#formcontactoI input,#formcontactoI textarea,input[type=checkbox]').on('keyup blur click', function (){
        if ($('#nombreI').hasClass('valid') && $('#emailI').hasClass('valid') && $('#telefonoI').hasClass('valid') && $('#asuntoI').hasClass('valid') && $('#mensajeI').hasClass('valid') && $('#formcontactoI input[type=checkbox]:checked').length > 0){
            $('#formcontactoI #su_contactI').prop('disabled', false);
        } else {
            $('#formcontactoI #su_contactI').prop('disabled', 'disabled');
        }
    });
    $('#formsuscribe input').on('keyup blur click', function (){
        if ($('#emailfooter').hasClass('valid') ){
            $('#formsuscribe #su_contactfotter').prop('disabled', false);
        } else {
            $('#formsuscribe #su_contactfotter').prop('disabled', 'disabled');
        }
    });
    $('#formcontacto input,#formcontactoI input').bind('focusin focusout keyup',function(eventObject){
        if(!$(this).val() || ($(this).val().length<2)){
            $(this).addClass('invalid');
            $(this).removeClass('valid');
            $(this).siblings('span.novalid').show();
            $(this).siblings('span.isvalid').hide();
        }else{
            $(this).removeClass('invalid');
            $(this).addClass('valid');
            $(this).siblings('span.novalid').hide();
            $(this).siblings('span.isvalid').show();
        }
    });
    $('#formcontacto textarea,#formcontactoI textarea').bind('focusin focusout keyup',function(eventObject){
        if(!$(this).val() || ($(this).val().length<20)){
            $(this).addClass('invalid');
            $(this).removeClass('valid');
            $(this).siblings('span.novalid').show();
            $(this).siblings('span.isvalid').hide();
        }else{
            $(this).removeClass('invalid');
            $(this).addClass('valid');
            $(this).siblings('span.novalid').hide();
            $(this).siblings('span.isvalid').show();
        }
    });
    $("#telefono,#telefonoI").bind('focusout keyup',function(event){
        if(!$(this).val() || ($(this).val().length<15)){//si esto no es valido
            $(this).addClass('invalid');
            $(this).removeClass('valid');
            $(this).siblings('span.novalid').show();
            $(this).siblings('span.isvalid').hide();
        }else{
            $(this).removeClass('invalid');
            $(this).addClass('valid');
            $(this).siblings('span.novalid').hide();
            $(this).siblings('span.isvalid').show();
        }
    });
    $("#email,#emailI,#emailpegajoso,#emailaleatorio,#emailfooter").bind("keypress", function (event) {//caracteres validos
        if (event.charCode!=0) {
            var regex = new RegExp("^[a-zA-Z0-9@&'*+-._]$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
    });
    $("#email,#emailI,#emailpegajoso,#emailaleatorio,#emailfooter").bind('focusout keyup',function(eventObject){
        var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
        if(!expresion.test($(this).val())){
            $(this).addClass('invalid');
            $(this).removeClass('valid');
            $(this).siblings('span.novalid').show();
            $(this).siblings('span.isvalid').hide();
        }else{
            $(this).removeClass('invalid');
            $(this).addClass('valid');
            $(this).siblings('span.novalid').hide();
            $(this).siblings('span.isvalid').show();
        }
    });    
});
/**/
$('#formcontacto #su_contact').on('click', function (){
    if ($('#nombre').hasClass('valid') && $('#email').hasClass('valid') && $('#telefono').hasClass('valid') && $('#asunto').hasClass('valid') && $('#mensaje').hasClass('valid') && $('#formcontacto input[type=checkbox]:checked').length > 0){
        if ($('#nombre').val().length >=2 && $('#email').val().length >=2 && $('#telefono').val().length >=2 && $('#asunto').val().length >=2 && $('#mensaje').val().length >=2 && $('#formcontacto input[type=checkbox]:checked').length > 0){
            var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
	        if(expresion.test($('#email').val())){
                var url = window.location.href;
                $.ajax({
                    url: url + "contacto",
                    method:"post",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data:{"data": btoa(Utf8.encode($('#formcontacto').serialize()))},
                    timeout: 60000,
                    beforeSend:function(){
                        $('#formcontacto #su_contact').prop('disabled', 'disabled');
                        $('#sending').show();
                    },
                    success:function(respuesta){
                        $('#formcontacto input').removeClass('valid');
                        $('#formcontacto textarea').removeClass('invalid');
                        $('#formcontacto input.form-control').val('');
                        $('#formcontacto textarea.form-control').val('');
                        $('#formcontacto .spanvalidate').hide();
                        $('#sending .evn').hide();
                        $('#sending .mensaje').html('<h5>'+respuesta.mensaje+'</h5>');
                        $('#sending .mensaje').show();
                        $('#formcontacto #su_contact').prop('disabled', 'disabled');
                        $('#dismis').show();
                        $('#formcontacto input[type=checkbox]').prop('checked', false);
                        setTimeout(function(){
                            $('#sending').hide();
                            $('#dismis').hide();
                            $('#sending .evn').show();
                            $('#sending .mensaje').hide();
                            $('#sending .mensaje').html('');
                        },3000);
                    },
                    error:function(e){
                       $('#sending .evn').hide();
                        $('#sending .mensaje').show();
                        $('#sending .mensaje').html('<h5>'+e.responseJSON.mensaje+'</h5>');
                        setTimeout(function(){
                            $('#sending').hide();
                            $('#dismis').hide();
                            $('#sending .evn').show();
                            $('#sending .mensaje').hide();
                            $('#sending .mensaje').html('');
                        },3000);
                        $('#formcontacto #su_contact').prop('disabled', false);
                    }
                });
            }
        }
    }
});
/**/
$('#formcontactoI #su_contactI').on('click', function (){
    if ($('#nombreI').hasClass('valid') && $('#emailI').hasClass('valid') && $('#telefonoI').hasClass('valid') && $('#asuntoI').hasClass('valid') && $('#mensajeI').hasClass('valid') && $('#formcontactoI input[type=checkbox]:checked').length > 0){
        if ($('#nombreI').val().length >=2 && $('#emailI').val().length >=2 && $('#telefonoI').val().length >=2 && $('#asuntoI').val().length >=2 && $('#mensajeI').val().length >=2 && $('#formcontactoI input[type=checkbox]:checked').length > 0){
            var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
	        if(expresion.test($('#emailI').val())){
                var url = window.location.href;
                $.ajax({
                    url: url + "contacto",
                    method:"post",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data:{"data": btoa(Utf8.encode($('#formcontactoI').serialize()))},
                    timeout: 60000,
                    beforeSend:function(){
                        $('#formcontactoI #su_contactI').prop('disabled', 'disabled');
                        $('#sendingI').show();
                    },
                    success:function(respuesta){
                        $('#formcontactoI input').removeClass('valid');
                        $('#formcontactoI textarea').removeClass('invalid');
                        $('#formcontactoI input.form-control').val('');
                        $('#formcontactoI textarea.form-control').val('');
                        $('#formcontactoI .spanvalidate').hide();
                        $('#sendingI .evn').hide();
                        $('#sendingI .mensaje').html('<h5>'+respuesta.mensaje+'</h5>');
                        $('#sendingI .mensaje').show();
                        $('#formcontactoI #su_contactI').prop('disabled', 'disabled');
                        $('#dismisI').show();
                        $('#formcontactoI input[type=checkbox]').prop('checked', false);
                        setTimeout(function(){
                            $('#sendingI').hide();
                            $('#dismisI').hide();
                            $('#sendingI .evn').show();
                            $('#sendingI .mensaje').hide();
                            $('#sendingI .mensaje').html('');
                        },3000);
                    },
                    error:function(e){
                       $('#sendingI .evn').hide();
                        $('#sendingI .mensaje').show();
                        $('#sendingI .mensaje').html('<h5>'+e.responseJSON.mensaje+'</h5>');
                        setTimeout(function(){
                            $('#sendingI').hide();
                            $('#dismisI').hide();
                            $('#sendingI .evn').show();
                            $('#sendingI .mensaje').hide();
                            $('#sendingI .mensaje').html('');
                        },3000);
                        $('#formcontactoI #su_contactI').prop('disabled', false);
                    }
                });
            }
        }
    }
});
/**/
$(window).on('load', function () {
    setTimeout(function(){
        $('#suscribealeatorio').modal('show');
    },10000);
});
// add the animation to the modal
$("#suscribealeatorio.modal").each(function(index) {
    $(this).on('show.bs.modal', function(e) {
        var open = $(this).attr('data-easein');
        $('.modal-dialog').velocity('callout.' + open);
    });
});
/**/
/*$('.popover-dismiss').popover({
    trigger: 'focus'
})*/
$('.sharenews').on('click',function(){
    $('#closepopover').click();
})
/**/
$(".toContacproductos").on('click',function(event){
    event.preventDefault();
    $('#filterstore').val($(this).attr('href'));
    if($(this).attr('href') == 'converse'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('converse', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        })
    }
    if($(this).attr('href') == 'everlast'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('everlast', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        })
    }
    if($(this).attr('href') == 'multimarca'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('multimarca', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        }); 
    }
    tiendasswiper =  new Swiper('#tiendasswiper', {
        slidesPerView: 2,
        slidesPerColumn: 3,
        spaceBetween: 0,
        slidesPerColumnFill: 'column',
        keyboard: {
            enabled: true,
        },
        pagination: {
            el: '#tiendasswiper .swiper-pagination',
            clickable: true,
        },
        resizeReInit: true,
        breakpoints: {
            1299: {
                slidesPerView: 2,
                slidesPerColumn: 2
            },
            991: {
                slidesPerView: 1,
                slidesPerColumn: 2
            },
            /*575: {
                slidesPerView: 1,
                slidesPerColumn: 3
            },*/
        }
    });    
    swiperdelsay.slideTo($(this).data('swiper'),700,function(){
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    }); 
});
$(".toContac,.footerto").on('click',function(event){
    event.preventDefault();
    swiperdelsay.slideTo($(this).data('swiper'),700,function(){
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    });
    /*controller.scrollTo($(this).attr('href'));*/   
});
$(".slideTo").on('click',function(event){
    event.preventDefault();
    if($('#ubicacion').is(':visible')){
        $('#close2').click();
    }
    if($('#noticia').is(':visible')){
        $('#close1').click();
    }
    if($('#acercade').is(':visible')){
        $('#acercade').velocity({translateX: '100%' }, { display:"none",duration:1000 });
    }
    swiperdelsay.slideTo($(this).data('swiper'),700,function(){
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    });
    if($('.toggle-button').hasClass('button-open'))
        setTimeout(function(){
            $('.toggle-button').click();
        },300);
    /*controller.scrollTo($(this).attr('href'));*/
});
$('#hight1').on('click',function(event){
    if($('#ubicacion').is(':visible')){
        $('#close2').click();
    }
    if($('#noticia').is(':visible')){
        $('#close1').click();
    }
    if($('#acercade').is(':visible')){
        $('#acercade').velocity({translateX: '100%' }, { display:"none",duration:1000 });
    }
    swiperdelsay.slideTo($(this).data('swiper'),700,function(){
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    });
    setTimeout(function(){
        $('.toggle-button').click();
    },300);   
    /*controller.scrollTo('#productos');*/
});
//Noicias
$('.open').click(function (event) {
    event.preventDefault();
    localforage.getItem($(this).attr('href')).then(function(readValue) {
        var carouselimg = "";
        var carouselindicators = "";
        var url = window.location.href;
        var route ="img/noticias/";
        carouselindicators += "<li data-target=\"#newscarousel\" data-slide-to=\"0\" class=\"active\"></li>";
        carouselimg += "<div class=\"w-100 h-100 carousel-item active\"><a class=\"\" href=\""+url+route+readValue.images1+"\" data-lightbox=\"news-set\" data-title=\"\"><img class=\"d-block mx-auto\" src=\""+url+route+readValue.images1+"\" alt=\"Second slide\"\></a></div>";
        if(readValue.images2 != ""){
            carouselindicators += "<li data-target=\"#newscarousel\" data-slide-to=\"1\"></li>";
            carouselimg += "<div class=\"w-100 h-100 carousel-item\"><a class=\"\" href=\""+url+route+readValue.images2+"\" data-lightbox=\"news-set\" data-title=\"\"><img class=\"d-block mx-auto\" src=\""+url+route+readValue.images2+"\" alt=\"Second slide\"\></a></div>";
        }
        if(readValue.images3 != ""){
            carouselindicators += "<li data-target=\"#newscarousel\" data-slide-to=\"2\"></li>";
            carouselimg += "<div class=\"w-100 h-100 carousel-item\"><a class=\"\" href=\""+url+route+readValue.images3+"\" data-lightbox=\"news-set\" data-title=\"\"><img class=\"d-block mx-auto\" src=\""+url+route+readValue.images3+"\" alt=\"Second slide\"\></a></div>";
        }
        $("#headernews>div[class*='col']:nth-child(1)>p").text(readValue.littletitle.toUpperCase());
        $("#headernews>div[class*='col']:nth-child(2)>h5").text(readValue.bigtitle.toUpperCase());
        $("#headernews>div[class*='col']:nth-child(3)>p").text(readValue.date);
        $("#newspaper").html(readValue.paragraphs);
        $("#newscarousel .carousel-inner").html(carouselimg);
        $("#newscarousel .carousel-indicators").html(carouselindicators);
        $('#noticia').velocity({translateY: ['0%','100%'] }, { display:"block",duration:1000 });
    }).catch(function(err) {
        console.log(err);
    });
});
$('#close1').click(function() {
    $('#noticia').velocity({translateY: '100%' }, { display:"none",duration:1000 });
});
$('#viewimg').click(function(event) {
    event.preventDefault();
    $('#newscarousel .carousel-inner>.carousel-item:nth-child(1) img').click();
});
//Ubucacion Tiendas
$('#close2').click(function() {
    $('#ubicacion').velocity({translateX: '100%' }, { display:"none",duration:1000 });
});
$('.locatorstore').click(function () {
    var urlgoogle = $(this).data('urlgoogle');
    var address = $(this).data('address');
    var titulostore = $(this).data('titulostore');
    var latitud = $(this).data('latitud');
    var longitud = $(this).data('longitud');
    initMap(urlgoogle,address,titulostore,latitud,longitud);    
    $('#ubicacion').velocity({translateX: ['0%','100%'] }, { display:"block",duration:1000 });
});
//suscripcion
$('#close-suscripcionpegajoso').click(function() {
    $('#suscripcionpegajoso').velocity({translateX: '100%' }, { display:"none",duration:1000 });
});
$('#btnsuscripcion').click(function (event) {
    event.preventDefault();
    $('#suscripcionpegajoso').velocity({translateX: ['0%','100%'] }, { display:"block",duration:1000 }); 
});
//Acerca de
$('.acercade').click(function (event) {
    event.preventDefault();
    
    $('#navcontend > div.row > ul[class*="col"] > li > a').removeClass('active');
    $('#acd').addClass('active');

    if($('#acercade').is(':visible')){
        setTimeout(function(){
            $('.toggle-button').click();
        },100);
        //cambiar pestana
        $('#acercade .nav-tabs a[href="'+$(this).attr('href')+'"]').tab('show');
        return false;
    }
    setTimeout(function(){
        $('.toggle-button').click();
    },100);
    //cambiar pestana
    $('#acercade .nav-tabs a[href="'+$(this).attr('href')+'"]').tab('show');
    setTimeout(function(){
        $('#acercade').velocity({translateX: ['0%','100%'] }, { display:"block",duration:1000 }); 
    },300);
});
$('#nav-filosofia-tab').on('shown.bs.tab', function (e) {
    $('#ccontatad').show();
});
$('#nav-filosofia-tab').on('hide.bs.tab', function (e) {
    $('#ccontatad').hide();
});
//footer
$('.footeracercade').click(function (event) {
    event.preventDefault();
    $('#navcontend > div.row > ul[class*="col"] > li > a').removeClass('active');
    $('#acd').addClass('active');
    if($('#acercade').is(':visible')){
        setTimeout(function(){
            $('.toggle-button').click();
        },100);
        //cambiar pestana
        $('#acercade .nav-tabs a[href="'+$(this).attr('href')+'"]').tab('show');
        return false;
    }
    //cambiar pestana
    $('#acercade .nav-tabs a[href="'+$(this).attr('href')+'"]').tab('show');
    setTimeout(function(){
        $('#acercade').velocity({translateX: ['0%','100%'] }, { display:"block",duration:1000 }); 
    },300);
});
//tiendas
$('#filterstore').on('change', function() {
    if(this.value == 'all'){
        $(".slidestore").addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show(); 
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        })
    }
    if(this.value == 'converse'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('converse', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        })
    }
    if(this.value == 'everlast'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('everlast', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        })
    }
    if(this.value == 'kimbow'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('kimbow', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        })
    }
    if(this.value == 'multimarca'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('multimarca', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        }); 
    }
    tiendasswiper =  new Swiper('#tiendasswiper', {
        slidesPerView: 2,
        slidesPerColumn: 3,
        spaceBetween: 0,
        slidesPerColumnFill: 'column',
        keyboard: {
            enabled: true,
        },
        pagination: {
            el: '#tiendasswiper .swiper-pagination',
            clickable: true,
        },
        resizeReInit: true,
        breakpoints: {
            1299: {
                slidesPerView: 2,
                slidesPerColumn: 2
            },
            991: {
                slidesPerView: 1,
                slidesPerColumn: 2
            },
            /*575: {
                slidesPerView: 1,
                slidesPerColumn: 3
            },*/
        }
    });
    setTimeout(function(){
        swiperdelsay.slideTo('4',50,function(){
            $("div.lazyload").lazyload({
                effect: "fadeIn",
                effectTime: 300,
                threshold: 2000
            });  
        });  
        $('#pagination span:nth-child(1)').click();
    },300);    
});
//logomarcas
$('.lilogoproductos').on('click',function(event) {
    event.preventDefault();
    $('#filterstore').val($(this).attr('href'));
    if($(this).attr('href') == 'converse'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('converse', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        })
    }
    if($(this).attr('href') == 'everlast'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('everlast', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        })
    }
    if($(this).attr('href') == 'multimarca'){
        $('.slidestore').each(function (index, value){
            var filter = $(value).data("filter").split(",");
            if(jQuery.inArray('multimarca', filter) !== -1)
                $(value).addClass("swiper-slide").removeClass("non-swiper-slide").attr("style", null).show();
            else
                $(value).addClass("non-swiper-slide").removeClass("swiper-slide").hide();
        });
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        }); 
    }
    tiendasswiper =  new Swiper('#tiendasswiper', {
        slidesPerView: 2,
        slidesPerColumn: 3,
        spaceBetween: 0,
        slidesPerColumnFill: 'column',
        keyboard: {
            enabled: true,
        },
        pagination: {
            el: '#tiendasswiper .swiper-pagination',
            clickable: true,
        },
        resizeReInit: true,
        breakpoints: {
            1299: {
                slidesPerView: 2,
                slidesPerColumn: 2
            },
            991: {
                slidesPerView: 1,
                slidesPerColumn: 2
            },
            /*575: {
                slidesPerView: 1,
                slidesPerColumn: 3
            },*/
        }
    });    
    swiperdelsay.slideTo($(this).data('swiper'),700,function(){
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    }); 
    setTimeout(function(){
        $('.toggle-button').click();
    },300);
    /*controller.scrollTo('#tiendas');*/
    /*swiperdelsay.slideTo(4,700,function(){
        $("div.lazyload").lazyload({
            effect: "fadeIn",
            effectTime: 300,
            threshold: 2000
        });
    });*/
});
/******/
function initMap(urlgoogle,address,titulostore,latitud,longitud) {
	var delsay = {lat: latitud, lng: longitud};
	var contentString = '<div style="display: inline-block; overflow: auto; max-height: 654px; max-width: 654px;">\
	<div dir="ltr" style="" jstcache="0">\
		<div jstcache="33" class="poi-info-window gm-style">\
			<div jstcache="2">\
				<div jstcache="3" class="title full-width" jsan="7.title,7.full-width">\
					<div class="gTitle"></div>\
				</div>\
				<div class="address">\
					<div jstcache="4" jsinstance="0" class="address-line full-width" jsan="7.address-line,7.full-width">\
                        <h4 style="text-align: center;">'+titulostore+'</h4>\
                        <p>'+address+'</p>\
					</div>\
				</div>\
			</div>\
			<div jstcache="5" style="display:none"></div>\
			<div class="view-link" style="text-align: center;">\
				<a target="_blank" jstcache="6" href="'+urlgoogle+'">\
					<span>Ver en Google Maps</span>\
				</a>\
			</div>\
		</div>\
		</div>\
	</div>';
	var map = new google.maps.Map(document.getElementById('map'), {
		center: delsay,
		zoom: 18
	});
	var marker = new google.maps.Marker({
		position: delsay,
		map: map,
		title: titulostore
	});
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	marker.addListener('click', function() {
		infowindow.open(map, marker);
	});
	infowindow.open(map,marker);	
}
/**/
$('#suscripcionpegajoso').on('submit',function(event){
    event.preventDefault();
    if ($('#emailpegajoso').hasClass('valid')){
        var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
        if(expresion.test($('#emailpegajoso').val())){
            var url = window.location.href;
            $.ajax({
                url: url + "suscribe",
                method:"post",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{"data": btoa(Utf8.encode($('#emailpegajoso').val()))},
                timeout: 60000,
                beforeSend:function(){
                    $('#suscripcionpegajoso').prop('disabled', 'disabled');
                    $('#sendingpegajoso').show();
                },
                success:function(respuesta){
                    $('#suscripcionpegajoso').prop('disabled', false);
                    $('#suscripcionpegajoso input').removeClass('valid');
                    $('#suscripcionpegajoso input').val('');
                    $('#suscripcionpegajoso .spanvalidate').hide();
                    $('#sendingpegajoso .evn').hide();
                    $('#sendingpegajoso .mensaje').html('<h5>'+respuesta.mensaje+'</h5>');
                    $('#sendingpegajoso .mensaje').show();
                    $('#dismispegajoso').show();
                    setTimeout(function(){
                        $('#sendingpegajoso').hide();
                        $('#suscripcionpegajoso').velocity({translateX: '100%' }, { display:"none",duration:1000 });
                        $('#dismispegajoso').hide();
                        $('#sendingpegajoso .evn').show();
                        $('#sendingpegajoso .mensaje').hide();
                        $('#sendingpegajoso .mensaje').html('');
                    },3000);
                },
                error:function(e){
                    $('#suscripcionpegajoso').prop('disabled', false);
                    $('#sendingpegajoso .evn').hide();
                    $('#sendingpegajoso .mensaje').show();
                    $('#sendingpegajoso.mensaje').html('<h5>'+e.responseJSON.mensaje+'</h5>');
                    setTimeout(function(){
                        $('#sendingpegajoso').hide();
                        $('#dismispegajoso').hide();
                        $('#sendingpegajoso .evn').show();
                        $('#sendingpegajoso .mensaje').hide();
                        $('#sendingpegajoso .mensaje').html('');
                    },3000);
                }
            });
        }
    }
});
/**/
$('#suscribealeatorio').on('submit',function(event){
    event.preventDefault();
    if ($('#emailaleatorio').hasClass('valid')){
        var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
        if(expresion.test($('#emailaleatorio').val())){
            var url = window.location.href;
            $.ajax({
                url: url + "suscribe",
                method:"post",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{"data": btoa(Utf8.encode($('#emailaleatorio').val()))},
                timeout: 60000,
                beforeSend:function(){
                    $('#suscribealeatorio').prop('disabled', 'disabled');
                    $('#sendingpegajosoI').show();
                },
                success:function(respuesta){
                    $('#suscribealeatorio').prop('disabled', false);
                    $('#suscribealeatorio input').removeClass('valid');
                    $('#suscribealeatorio input').val('');
                    $('#suscribealeatorio .spanvalidate').hide();
                    $('#sendingpegajosoI .evn').hide();
                    $('#sendingpegajosoI .mensaje').html('<h5>'+respuesta.mensaje+'</h5>');
                    $('#sendingpegajosoI .mensaje').show();
                    $('#dismispegajosoI').show();
                    setTimeout(function(){
                        $('#sendingpegajosoI').hide();
                        $('#suscribealeatorio').modal('hide');
                        $('#dismispegajosoI').hide();
                        $('#sendingpegajosoI .evn').show();
                        $('#sendingpegajosoI .mensaje').hide();
                        $('#sendingpegajosoI .mensaje').html('');
                    },3000);
                },
                error:function(e){
                    $('#suscribealeatorio').prop('disabled', false);
                    $('#sendingpegajosoI .evn').hide();
                    $('#sendingpegajosoI .mensaje').html('<h5>'+e.responseJSON.mensaje+'</h5>');
                    $('#sendingpegajosoI .mensaje').show();
                    setTimeout(function(){
                        $('#sendingpegajosoI').hide();
                        $('#dismispegajosoI').hide();
                        $('#sendingpegajosoI .evn').show();
                        $('#sendingpegajosoI .mensaje').hide();
                        $('#sendingpegajosoI .mensaje').html('');
                    },3000);
                }
            });
        }
    }
});
/**/
//$('#formsuscribe #su_contactfotter').on('click', function (event){
$('#formsuscribe').on('submit',function(event){    
    event.preventDefault();
    if ($('#emailfooter').hasClass('valid')){
        var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
        if(expresion.test($('#emailfooter').val())){
            var url = window.location.href;
            $.ajax({
                url: url + "suscribe",
                method:"post",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{"data": btoa(Utf8.encode($('#emailfooter').val()))},
                timeout: 60000,
                beforeSend:function(){
                    $('#suscribealeatorio').prop('disabled', 'disabled');
                    $('#sendingspegajoso').show();
                },
                success:function(respuesta){
                    $('#su_contactfotter').prop('disabled', false);
                    $('#formsuscribe input').removeClass('valid');
                    $('#formsuscribe input[type="email"]').val('');
                    $('#formsuscribe .spanvalidate').hide();
                    $('#sendingspegajoso .evn').hide();
                    $('#sendingspegajoso .mensaje').html('<h5>'+respuesta.mensaje+'</h5>');
                    $('#sendingspegajoso .mensaje').show();
                    $('#dismispegajosoI').show();
                    setTimeout(function(){
                        $('#sendingspegajoso').hide();
                        $('#sendingspegajoso .evn').show();
                        $('#sendingspegajoso .mensaje').hide();
                        $('#sendingspegajoso .mensaje').html('');
                    },3000);
                },
                error:function(e){
                    $('#su_contactfotter').prop('disabled', false);
                    $('#sendingspegajoso .evn').hide();
                    $('#sendingspegajoso .mensaje').show();
                    $('#sendingspegajoso .mensaje').html('<h5>'+e.responseJSON.mensaje+'</h5>');
                    setTimeout(function(){
                        $('#sendingspegajoso').hide();
                        $('#dismispegajosoI').hide();
                        $('#sendingspegajoso .evn').show();
                        $('#sendingspegajoso .mensaje').hide();
                        $('#sendingspegajoso .mensaje').html('');
                    },3000);
                }
            });
        }
    }
});/****/
/*Utf8 encode function*/
var Utf8 ={
    // public method for url encoding
    encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128){
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)){
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }else{
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },
    // public method for url decoding
    decode : function (utftext){
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while ( i < utftext.length ){
            c = utftext.charCodeAt(i);
            if (c < 128){
                string += String.fromCharCode(c);
                i++;
            }else if((c > 191) && (c < 224)){
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }else{
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}