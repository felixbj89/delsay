$("div.lazyload").lazyload({
    effect: "fadeIn",
    effectTime: 300,
    threshold: 2000
});
$("#emailfooter").bind('focusout keyup',function(eventObject){
    var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
    if(!expresion.test($(this).val())){
        $(this).addClass('invalid');
        $(this).removeClass('valid');
        $(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
    }else{
        $(this).removeClass('invalid');
        $(this).addClass('valid');
        $(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
    }
}); 
$('#formsuscribe input').on('keyup blur click', function (){
    if ($('#emailfooter').hasClass('valid') ){
        $('#formsuscribe #su_contactfotter').prop('disabled', false);
    } else {
        $('#formsuscribe #su_contactfotter').prop('disabled', 'disabled');
    }
});
$('#su_contactfotter').on('click', function (event){
    event.preventDefault();
    if ($('#emailfooter').hasClass('valid')){
        var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
        if(expresion.test($('#emailfooter').val())){
            var url = window.location.href.substr(0,window.location.href.search("marca"));
            $.ajax({
                url: url + "suscribe",
                method:"post",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{"data": btoa(Utf8.encode($('#emailfooter').val()))},
                timeout: 60000,
                beforeSend:function(){
                    $('#suscribealeatorio').prop('disabled', 'disabled');
                    $('#sendingspegajoso').show();
                },
                success:function(respuesta){
                    $('#su_contactfotter').prop('disabled', false);
                    $('#formsuscribe input').removeClass('valid');
                    $('#formsuscribe input[type="email"]').val('');
                    $('#formsuscribe .spanvalidate').hide();
                    $('#sendingspegajoso .evn').hide();
                    $('#sendingspegajoso .mensaje').html('<h5>'+respuesta.mensaje+'</h5>');
                    $('#sendingspegajoso .mensaje').show();
                    $('#dismispegajosoI').show();
                    setTimeout(function(){
                        $('#sendingspegajoso').hide();
                        $('#sendingspegajoso .evn').show();
                        $('#sendingspegajoso .mensaje').hide();
                        $('#sendingspegajoso .mensaje').html('');
                    },3000);
                },
                error:function(e){
                    $('#su_contactfotter').prop('disabled', false);
                    $('#sendingspegajoso .evn').hide();
                    $('#sendingspegajoso .mensaje').show();
                    $('#sendingspegajoso .mensaje').html('<h5>'+e.responseJSON.mensaje+'</h5>');
                    setTimeout(function(){
                        $('#sendingspegajoso').hide();
                        $('#dismispegajosoI').hide();
                        $('#sendingspegajoso .evn').show();
                        $('#sendingspegajoso .mensaje').hide();
                        $('#sendingspegajoso .mensaje').html('');
                    },3000);
                }
            });
        }
    }
});
/**/
/*Utf8 encode function*/
var Utf8 ={
    // public method for url encoding
    encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128){
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)){
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }else{
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },
    // public method for url decoding
    decode : function (utftext){
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while ( i < utftext.length ){
            c = utftext.charCodeAt(i);
            if (c < 128){
                string += String.fromCharCode(c);
                i++;
            }else if((c > 191) && (c < 224)){
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }else{
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}