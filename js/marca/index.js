var prinbanner = new Swiper('#prinbanner', {
    preloadImages: false,
    lazy: true,    
    spaceBetween: 10,
    centeredSlides: true,
    speed: 700,
    autoplay: {
        delay: 9000,
        disableOnInteraction: false,
    },
    resizeReInit: true,
    pagination: {
        el: '#prinbanner .swiper-pagination',
        clickable: true,
    },
});
var pop = new Swiper('#pop', {
    loop: true,
    slidesPerView: 4,
    spaceBetween: 0,
    mousewheel: true,
    pagination: {
        el: '#pop .swiper-pagination',
        clickable: true,
    },
    resizeReInit: true,
    breakpoints: {
        1299: {
            slidesPerView: 4,
        },
        991: {
            slidesPerView: 3,
        },
        767: {
            slidesPerView: 2,
        },     
        575: {
            slidesPerView: 1,
        }
    }
});
var publibanner = new Swiper('#publibanner', {
    preloadImages: false,
    lazy: true,
    effect: 'fade',
    spaceBetween: 10,
    centeredSlides: true,
    speed: 700,
    autoplay: {
        delay: 9000,
        disableOnInteraction: false,
    },
    resizeReInit: true,
    pagination: {
        el: '#publibanner .swiper-pagination',
        clickable: true,
    },
});
var venta = new Swiper('#venta', {
    loop: true,
    slidesPerView: 4,
    spaceBetween: 0,
    mousewheel: true,
    pagination: {
        el: '#venta .swiper-pagination',
        clickable: true,
    },
    resizeReInit: true,
    breakpoints: {
        1299: {
            slidesPerView: 4,
        },
        991: {
            slidesPerView: 3,
        },
        767: {
            slidesPerView: 2,
        },     
        575: {
            slidesPerView: 1,
        }
    }
});
var red = new Swiper('#red', {
    slidesPerView: 5,
    spaceBetween: 0,
    loop: true,
    pagination: {
        el: '#red .swiper-pagination',
        clickable: true,
    },
    navigation: {
      nextEl: '#red .swiper-button-next',
      prevEl: '#red .swiper-button-prev',
    },
    resizeReInit: true,
    breakpoints: {
        1299: {
            slidesPerView: 5,
        },
        991: {
            slidesPerView: 4,
        },
        767: {
            slidesPerView: 3,
        },     
        575: {
            slidesPerView: 2,
        }
    }
});
pop.on('slideChange', function () { 
    $("div.lazyload").lazyload({
        effect: "fadeIn",
        effectTime: 300,
        threshold: 2000
    });
});
venta.on('slideChange', function () { 
    $("div.lazyload").lazyload({
        effect: "fadeIn",
        effectTime: 300,
        threshold: 2000
    });
});