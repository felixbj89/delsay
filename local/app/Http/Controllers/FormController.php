<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use App\MailChimp;

class FormController extends Controller
{
    public function indexsuscribe (Request $request){
        $data = urldecode(base64_decode($request->data));
        if (!preg_match("/^[a-z0-9_\.\-]+(.[a-z0-9_\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/",$data)){
            return response()->json([
                "respuesta" => $data,
                "mensaje" => \Lang::get('mensajes.error_email')
            ],409);
        }
        $MailChimp = new MailChimp(base64_decode(\Config::get('subs.mailchimp.key')));
        $menber = $MailChimp->subscriberHash($data);
        $subscriber = $MailChimp->get('lists/'.\Config::get('subs.mailchimp.list_id').'/members/'. $menber);

        if($subscriber['status'] == 404){
            try{
                $route = url('/');
                $asunto = 'Suscripción a delsay';
                $pathToFile = $route."/img/logos/logodelsayemail.png";
                $isotipoconverse = $route."/img/logos/isotipo_converse_email.png";
                $isotipoeverlast = $route."/img/logos/isotipo_everlast_email.png";
                $isotipokimbow = $route."/img/logos/isotipo_kimbow_email.png";
                $isotipodelsay = $route."/img/logos/favicon_delsay.png";
                $parametrosemail = [$data,$asunto];
                Mail::send('email.suscribe', ["correo" => $parametrosemail[0],"pathToFile" => $pathToFile,"local" => $route,"converse" => $isotipoconverse,"everlast" => $isotipoeverlast,"kimbow" => $isotipokimbow,"isotipodelsay" => $isotipodelsay], function($message) use ($parametrosemail){
                    $message->to($parametrosemail[0])->subject($parametrosemail[1]);
                    $message->from("distribuidoradelsay@info.com", "Distribuidora delsay c.a");
                });
                $subscriber = $MailChimp->post("lists/".\Config::get('subs.mailchimp.list_id')."/members", [
                    'email_address' => $data,
                    'status'        => 'subscribed',
                ]);


            }catch(\Exception $e){
                return response()->json([
                    "respuesta" => $data,
                    "mensaje" => \Lang::get('mensajes.error_sendmail')
                ],500); 
            }
            if($subscriber['status'] == 'subscribed')
                return response()->json([
                    "respuesta" => $data,
                    "mensaje" => \Lang::get('mensajes.sus_sendmail')
                ],200); 
        }else{
            return response()->json([
                "respuesta" => $data,
                "mensaje" => \Lang::get('mensajes.sus_exists')
            ],200);
        }
    }

    public function indexcontacto (Request $request){
        $data = urldecode(base64_decode($request->data));
        $parts = explode("&",$data);
        $emails = [];
        foreach($parts as $part){
            $contac[explode("=",$part)['0']] = explode("=",$part)['1'];
        }
        if(count($contac) >= 6 && count($contac) <= 8){ 
            if (!preg_match("/^[a-z0-9_\.\-]+(.[a-z0-9_\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/",$contac['email'])){
				return response()->json([
                    "respuesta" => $contac,
                    "mensaje" => \Lang::get('mensajes.error_email')
                ],409);
            }
			$parametros = [
				"nombre" => $contac['nombre'],
				"correo" => $contac['email'],
				"telefono" => $contac['telefono'],
                "mensaje" => $contac['mensaje'],
                "ventas" => "",
                "mercadeo" => "",
                "recursos" => "",
			];
            if(array_key_exists('ventasCheckbox', $contac)){
                //array_push($emails, "paginawebdelsay@gmail.com");
                $parametros["ventas"] = "Departamento de Ventas";
            }
            if(array_key_exists('mercadeoCheckbox', $contac)){
                //array_push($emails, "paginawebdelsay@gmail.com");
                $parametros["mercadeo"] = "Departamento de Mercadeo";
            }
            if(array_key_exists('recursoshumanosCheckbox', $contac)){
                //array_push($emails, "paginawebdelsay@gmail.com");
                $parametros["recursos"] = "Departamento de Recursos Humanos";
            }
            array_push($emails, "paginawebdelsay@gmail.com");
            $asunto = 'Contacto #'.$contac['nombre']." - ".$contac['asunto'];
            $parametrosemail = [$asunto,$emails];
            try{
                Mail::send('email.contacto', $parametros, function($message) use($parametrosemail){
                    $message->to($parametrosemail[1])->subject($parametrosemail[0]);
                    $message->from("distribuidoradelsay@info.com", "Distribuidora delsay c.a");
                });
			}catch(\Exception $e){
                return response()->json([
                    "respuesta" => $contac,
                    "mensaje" => \Lang::get('mensajes.error_sendmail')
                ],500); 
            }
            return response()->json([
                "respuesta" => $contac,
                "mensaje" => \Lang::get('mensajes.sus_sendmail')
            ],200);  
        }else{
            return response()->json([
                "respuesta" => $contac,
                "mensaje" => \Lang::get('mensajes.error_parametros')
            ],409);
        }
	}
}