<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ROUTER FRONT 
Route::get('/', 'FrontController@index');

//FORMULARIOS
Route::post('contacto', 'FormController@indexcontacto');
Route::post('suscribe', 'FormController@indexsuscribe');

//MARCAS
Route::group(["prefix"  => "marca"], function () {
    Route::get('{brand}', ['uses' => 'MarcafrontController@index', 'as' => 'marca/{brand}']);
});