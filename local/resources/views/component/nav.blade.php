<div class="row no-gutters h-100 py-2">
    <div class="col-12 align-self-center px-2">
        <div class="row no-gutters">
            <div class="col-12 mb-4">
                <div class="row no-gutters d-flex mb-1">
                    <div class="col-12 col-sm-6 col-md-7 order-2 order-sm-1">
                        <a class="slideTo" @if(Request::is('/')) href="#banner" @else href="{{url('/')}}" @endif id="home"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="44.763px" height="51px" viewBox="0 0 44.763 51" enable-background="new 0 0 44.763 51" xml:space="preserve"><g><line fill="none" stroke="#FFFFFF" stroke-width="3" stroke-miterlimit="10" x1="43.56" y1="24.252" x2="1.203" y2="24.252"/><line fill="none" stroke="#FFFFFF" stroke-width="3" stroke-miterlimit="10" x1="33.153" y1="33.117" x2="1.203" y2="33.117"/></g></svg>
                        Inicio</a>
                        <form id="formbusqueda" class="form-inline">
                            <div class="form-group my-0 position-relative">
                                <input type="text" class="form-control form-control-sm" id="busqueda" placeholder="@lang('delsay.buscar')">
                                <button type="submit" class="btn-busqueda"></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-sm-6 col-md-5 order-1 order-sm-2">
                        <div class="row no-gutters h-100 delsaylogo"></div>
                    </div>
                </div>
            </div>
            <div id="navcontend" class="col-12">
                <div class="row no-gutters">
                    <ul class="col">
                        <li><a id="hight1" data-swiper="1" class="submenu" data-toggle="collapse" @if(Request::is('/')) href="#liproductos" @else href="{{url('/')}}" @endif role="button" aria-expanded="false" aria-controls="liproductos">@lang('delsay.navliI')</a></li>
                        <li><a id="hight2" data-swiper="2" class="slideTo nosubmenu" @if(Request::is('/')) href="#populares" @else href="{{url('/')}}" @endif >@lang('delsay.navliII')</a></li>                    
                        <li><a id="hight3" data-swiper="3" class="slideTo nosubmenu" @if(Request::is('/')) href="#noticias" @else href="{{url('/')}}" @endif >@lang('delsay.navliIII')</a></li>                    
                        <li><a id="hight4" data-swiper="4" class="slideTo nosubmenu" @if(Request::is('/')) href="#tiendas" @else href="{{url('/')}}" @endif >@lang('delsay.navliIV')</a></li>                    
                        <li>
                            <a id="acd" class="submenu" data-toggle="collapse" @if(Request::is('/')) href="#referencia" @else href="{{url('/')}}" @endif role="button" aria-expanded="false" aria-controls="referencia">@lang('delsay.navliV')</a>
                            <div id="line"><div id="hl"></div></div>
                        </li>
                        <li><a id="hight5" data-swiper="5" class="slideTo nosubmenu" @if(Request::is('/')) href="#contacto" @else href="{{url('/')}}" @endif >@lang('delsay.navliVI')</a></li>                    
                        <li><hr class="my-2 mx-0"/></li>
                        <li>
                            <div class="row no-gutters social">
                                <div class="col"><a id="hight6" data-swiper="6" class="slideTo nosubmenu" @if(Request::is('/')) href="#footer" @else href="{{url('/')}}" @endif >
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="42.631px" height="49.125px" viewBox="0 0 42.631 49.125" enable-background="new 0 0 42.631 49.125" xml:space="preserve"><g><g><g><path fill="#FFFFFF" d="M26.114,15.603l-2.487-0.004c-2.8,0-4.604,1.855-4.604,4.73v2.176h-2.506c-0.214,0-0.39,0.176-0.39,0.391 v3.156c0,0.215,0.176,0.391,0.39,0.391h2.506v7.965c0,0.215,0.173,0.393,0.392,0.393h3.265c0.216,0,0.393-0.178,0.393-0.393 v-7.965h2.926c0.218,0,0.393-0.176,0.393-0.391l0.002-3.156c0-0.105-0.042-0.203-0.12-0.277 c-0.067-0.074-0.168-0.115-0.274-0.115h-2.926v-1.85c0-0.885,0.209-1.336,1.365-1.336h1.682c0.212,0,0.389-0.176,0.389-0.391 v-2.932C26.506,15.778,26.33,15.603,26.114,15.603z"/></g></g><g><path fill="#FFFFFF" d="M21.316,45.2c-11.028,0-20-8.973-20-20s8.972-20,20-20s20,8.973,20,20S32.344,45.2,21.316,45.2z M21.316,6.532c-10.292,0-18.667,8.375-18.667,18.668s8.375,18.668,18.667,18.668c10.293,0,18.667-8.375,18.667-18.668 S31.609,6.532,21.316,6.532z"/></g></g></svg>
                                </a></div>
                                <div class="col"><a id="hight7" data-swiper="6" class="slideTo nosubmenu" @if(Request::is('/')) href="#footer" @else href="{{url('/')}}" @endif >
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="42.631px" height="49.125px" viewBox="0 0 42.631 49.125" enable-background="new 0 0 42.631 49.125" xml:space="preserve"><g><g><g><g><g><path fill="#FFFFFF" d="M25.23,16.474h-7.199c-3.313,0-6,2.688-6,6v7.201c0,3.313,2.688,6,6,6h7.199c3.314,0,6-2.688,6-6 v-7.201C31.23,19.161,28.544,16.474,25.23,16.474z M29.431,29.675c0,2.314-1.885,4.201-4.201,4.201h-7.199 c-2.316,0-4.201-1.887-4.201-4.201v-7.201c0-2.314,1.884-4.199,4.201-4.199h7.199c2.316,0,4.201,1.885,4.201,4.199V29.675z"/></g></g></g><g><g><g><path fill="#FFFFFF" d="M21.63,21.274c-2.65,0-4.8,2.148-4.8,4.801s2.149,4.801,4.8,4.801s4.801-2.148,4.801-4.801 S24.281,21.274,21.63,21.274z M21.63,29.075c-1.652,0-3-1.348-3-3c0-1.656,1.348-3,3-3c1.654,0,3,1.344,3,3 C24.63,27.728,23.285,29.075,21.63,29.075z"/></g></g></g><g><g><g><circle fill="#FFFFFF" cx="26.791" cy="20.915" r="0.641"/></g></g></g></g><g><path fill="#FFFFFF" d="M21.63,46.075c-11.027,0-20-8.973-20-20s8.973-20,20-20c11.028,0,20,8.973,20,20 S32.659,46.075,21.63,46.075z M21.63,7.407c-10.292,0-18.666,8.375-18.666,18.668s8.374,18.668,18.666,18.668 c10.293,0,18.668-8.375,18.668-18.668S31.923,7.407,21.63,7.407z"/></g></g></svg>
                                </a></div>
                                <div class="col"><a id="hight8" data-swiper="4" class="slideTo nosubmenu" @if(Request::is('/')) href="#tiendas" @else href="{{url('/')}}" @endif >
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="42.631px" height="49.125px" viewBox="0 0 42.631 49.125" enable-background="new 0 0 42.631 49.125" xml:space="preserve"><g><g><path fill="#FFFFFF" d="M19.773,9.157c-6.113,0-11.087,4.973-11.087,11.082c0,5.871,10.076,21.758,10.505,22.434 c0.127,0.197,0.347,0.32,0.582,0.32c0.236,0,0.456-0.123,0.582-0.32c0.429-0.676,10.506-16.563,10.506-22.434 C30.861,14.13,25.886,9.157,19.773,9.157z M19.773,40.997c-0.911-1.473-2.763-4.527-4.595-7.91 c-3.344-6.176-5.111-10.617-5.111-12.848c0-5.348,4.354-9.699,9.706-9.699c5.353,0,9.706,4.352,9.706,9.699 c0,2.23-1.767,6.672-5.11,12.848C22.536,36.47,20.685,39.524,19.773,40.997z"/></g><g><path fill="#FFFFFF" d="M19.773,14.411c-2.892,0-5.244,2.289-5.244,5.102s2.353,5.102,5.244,5.102 c2.893,0,5.245-2.289,5.245-5.102S22.666,14.411,19.773,14.411z M19.773,23.235c-2.13,0-3.864-1.672-3.864-3.723 s1.734-3.721,3.864-3.721c2.131,0,3.864,1.67,3.864,3.721S21.904,23.235,19.773,23.235z"/></g></g></svg>
                                </a></div>
                                <div class="col"></div>
                            </div>
                        </li>
                    </ul>
                    <div class="col">
                        <div class="collapse row" id="liproductos">
                            <div class="col mb-1"><a class="lilogoproductos" id="logoeverlast" title="Everlast" href="everlast"  data-swiper="4"></a></div>
                            <div class="col mb-1"><a class="lilogoproductos" id="logoconverse" title="Converse" href="converse" data-swiper="4"></a></div>
                            <div class="col mb-1"><a class="lilogoproductos" id="logokimdow" title="Kimbow" href="multimarca" data-swiper="4"></a></div>
                            <div class="col mb-1"><a class="lilogoproductos" id="logosportway" title="sport way" href="multimarca" data-swiper="4"></a></div>
                            <div class="col mb-1"><a class="lilogoproductos" id="logoonestore" title="one store" href="multimarca" data-swiper="4"></a></div>
                        </div>
                        <div class="collapse row" id="referencia">
                            <ul class="col">
                                <li><a class="acercade" href="#nav-empresa">@lang('delsay.navliiI')</a></li>
                                <li><a class="acercade" href="#nav-filosofia">@lang('delsay.navliiII')</a></li>
                                <li><a class="acercade" href="#nav-marcas">@lang('delsay.navliiIII')</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 my-2">
                        <a href="#">@lang('delsay.navliiiI')</a> | <a href="#">@lang('delsay.navliiiII')</a>
                    </div>
                    
                </div> 
            </div>
        </div>
    </div>
    <footer class="col-12 align-self-end px-2">
        <p>Distribuidora DelSay C.A - Sitio Oficial. Todos los Derechos Reservados 2018</p>
        <p>Desarrollado por <a id="hacienda" href="http://haciendacreativa.net/" target="_blank">Hacienda Creativa C.A</a></p>        
    </footer>
</div>