<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Contacto</title>
</head>
<body>
    <p>Hola! Se ha hecho un nuevo contacto.</p>
    <p>Estos son los datos del usuario que ha realizado el contacto:</p>
    <ul>
        <li>Dirigido a: <br>
            <ul>
                @if($ventas !== "")<li>{{e($ventas)}}</li>@endif
                @if($mercadeo !== "")<li>{{e($mercadeo)}}</li>@endif
                @if($recursos !== "")<li>{{e($recursos)}}</li>@endif
            </ul>
        </li>
    </ul>    
    <hr/>
    <ul>
        <li>Nombre: {{e($nombre)}}</li>
        <li>correo: {{e($correo)}}</li>
        <li>telefono: {{e($telefono)}}</li>
        <li>mensaje: {{e($mensaje)}}</li>
    </ul>
</body>
</html>