<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <!-- meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0" -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Welcome</title>
</head>
    <body bgcolor="#e2e2e2">
        <table border="0" cellpadding="0" cellspacing="0" style="background-color: #FFFFFF;" width="100%">
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="10" cellspacing="0" class="content" style="background-color: #FFFFFF;border: 1px solid #e2e2e2;margin-top: 0px;margin-right: auto;margin-bottom: 0px;margin-left: auto;" width="90%">
                        <tr>
                            <td id="templateContainerHeader" valign="top">
                                <p style="text-align:center;margin:0;padding:0;">
                                    <img src="{{ $message->embed($pathToFile) }}">
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" class="brdBottomPadd-two" id="templateContainer" width="100%">
                                    <tr>
                                        <td class="bodyContent" valign="top">
                                            <h3><strong>Hola!</strong>,</h3>

                                            <h1><strong>Felicitaciones! por registrarte <br>
                                            para Grupo DelSay</strong></h1>

                                            <h3>Gracias por unirse a nuestra comunidad. 
                                            Estamos muy contentos de compartir contigo una selección  de nuestras ultimas novedades a nivel nacional y productos que no te puedes perder.</h3>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="top">
                            <td class="bodyContentImage" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" class="brdBottomPadd-two" id="templateContainer" width="100%">
                                    <tr>
                                        <td class="bodyContent" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr valign="top">
                                                    <td valign="top" mc:edit="welcomeEditImgFirst" style="width:19%;font-size:14px;padding-bottom:1.143em;">
                                                        <p style=  "text-align:center;margin:0 0 15px 0;padding:0;"><img height="" src="{{ $message->embed($everlast) }}" style="display:block;" width="91"></p>
                                                    </td>
                                                    <td valign="top" style="width:5%;">&nbsp;</td>
                                                    <td valign="top"  style="width:71%;font-size:14px;padding-top:0.714em;">
                                                        <h3><strong>Everlast</strong></h3>
                                                        <p>Desde 1910, marca activa en el diseño, comercialización de ropa y accesorios deportivos. <strong>La Grandeza está dentro.</strong><a href="{{e($local)}}">Ver más</a></p>
                                                    </td>
                                                    <td valign="top" style="width:5%;">&nbsp;</td>
                                                </tr>
                                                <tr valign="top">
                                                    <td valign="top" mc:edit="welcomeEditImgFirst" style="width:19%;font-size:14px;padding-bottom:1.143em;">
                                                        <p style=  "text-align:center;margin:0 0 15px 0;padding:0;"><img height="" src="{{ $message->embed($converse) }}" style="display:block;" width="91"></p>
                                                    </td>
                                                    <td valign="top" style="width:5%;">&nbsp;</td>
                                                    <td valign="top"  style="width:71%;font-size:14px;padding-top:0.714em;">
                                                        <h3><strong>Converse</strong></h3>
                                                        <p>Más que zapatillas, Converse es la historia de leyendas, héroes y personajes innovadores, todos unidos por el amor al deporte. <strong>¡Converse, va con todo!</strong><a href="{{e($local)}}">Ver más</a></p>
                                                    </td>
                                                    <td valign="top" style="width:5%;">&nbsp;</td>
                                                </tr>
                                                <tr valign="top">
                                                    <td valign="top" mc:edit="welcomeEditImgFirst" style="width:19%;font-size:14px;padding-bottom:1.143em;">
                                                        <p style=  "text-align:center;margin:0 0 15px 0;padding:0;"><img height="" src="{{ $message->embed($kimbow) }}" style="display:block;" width="91"></p>
                                                    </td>
                                                    <td valign="top" style="width:5%;">&nbsp;</td>
                                                    <td valign="top"  style="width:71%;font-size:14px;padding-top:0.714em;">
                                                        <h3><strong>Kimbow</strong></h3>
                                                    <p>Marca con sello venezolano que fabrica textiles y uniformes para disciplinas deportivas, para damas, caballeros y niños.<a href="{{e($local)}}">Ver más</a></p>
                                                    </td>
                                                    <td valign="top" style="width:5%;">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p style="text-align:center;margin:0;padding:0;">
                                                <img src="{{ $message->embed($pathToFile) }}">
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="unSubContent" id="bodyCellFooter" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" id="templateContainerFooter" width="100%">
                        <tr>
                        <td valign="top" width="100%" mc:edit="welcomeEdit-11">
                            <p style="text-align:center;margin-top: 9px;margin-bottom:0px;">
                                <img src="{{$message->embed($isotipodelsay)}}"style="margin:0 auto 0 auto;display:inline-block;">
                            </p>
                            <h6 style="text-align:center;margin-top: 2px;margin-bottom:0px;color:#99979C">Distribuidora DelSay</h6>
                            <h6 style="text-align:center;margin-top:0px;margin-bottom:0px;color:#99979C">Caracas - Venezuela</h6>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <style type="text/css">

        
        </style>
    </body>
</html>