@extends("layout.master-brand")
@section("Mycss")
    <link href="{{ asset('css/marca/populares.css') }}" rel="stylesheet">
@endsection
@section("header-page")
    @include('frontend.marca.componetes.header')
@endsection
@section("body-page")
    <section id="banner" class="row no-gutters">
        @include('frontend.marca.componetes.bannerii')
    </section>
@endsection
@section("footer-page")
    @include('frontend.marca.componetes.footer')
@endsection
@section("Myscript")
    <script type="text/javascript" src="{{ asset('js/marca/populares.js') }}"></script>
@endsection