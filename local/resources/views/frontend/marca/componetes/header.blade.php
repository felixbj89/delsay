<div class="d-flex col-12 col-sm-11 mx-auto p-0">
    <div class="row no-gutters w-100 px-3 py-4 d-flex justify-content-center">
        <div class="col d-flex align-items-end pr-0 pr-sm-2 order-1 order-sm-0">
            <form id="formbusqueda" class="form-inline">
                    <div class="form-group my-0 position-relative">
                            <input type="text" class="form-control form-control-sm" id="busqueda" placeholder="@lang('delsay.buscar')">
                            <button type="submit" class="btn-busqueda"></button>
                    </div>
            </form>
        </div>
        <div class="col d-flex align-items-end order-0 order-sm-1">
            <div class="w-100 background" style="background-image:url({{asset('img/productos/converse_productos_hover.svg')}});"></div>
        </div>
        <div class="col order-2"></div>
    </div>
</div>