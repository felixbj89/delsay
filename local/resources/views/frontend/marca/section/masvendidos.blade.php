<div class="col-12">
    <h3 class="text-center text-uppercase font-weight-normal mb-0">los más vendidos</h3>
</div>
<div class="col-12 col-sm-11 mx-auto">
    <div id="venta" class="swiper-container"><!-- Swiper -->
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="venta w-100 d-flex justify-content-center align-items-center">
                    <div class="w-100 h-100 d-flex flex-column justify-content-center align-items-center">
                        <a href="" class="text-lowercase text-center font-weight-normal btn-marca-v">ver producto</a>
                    </div>
                    <div class="position-absolute w-100 px-1 py-2">
                        <div class="w-100 lazyload"
                        data-original="{{asset('img/marca/populares/Producto690_330.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                        </div>
                        <div class="w-100 d-flex justify-content-center align-items-center flex-column">
                            <h5 class="text-center font-weight-light m-0">Chuck Taylos All Star Clasisc Colors</h5>
                            <p class="text-center font-weight-bold m-0">65.000 Bs.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="venta w-100 d-flex justify-content-center align-items-center">
                    <div class="w-100 h-100 d-flex flex-column justify-content-center align-items-center">
                        <a href="" class="text-lowercase text-center font-weight-normal btn-marca-v">ver producto</a>
                    </div>
                    <div class="position-absolute w-100 px-1 py-2">
                        <div class="w-100 lazyload"
                        data-original="{{asset('img/marca/populares/Producto690_330.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                        </div>
                        <div class="w-100 d-flex justify-content-center align-items-center flex-column">
                            <h5 class="text-center font-weight-light m-0">Chuck Taylos All Star Clasisc Colors</h5>
                            <p class="text-center font-weight-bold m-0">65.000 Bs.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</div>