<div class="col-12">
    <h3 class="text-center text-uppercase font-weight-normal mb-0">populares</h3>
</div>
<div class="col-12 col-sm-11 mx-auto">
    <div id="pop" class="swiper-container"><!-- Swiper -->
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="pop w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="d-flex flex-column justify-content-between">
                        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">ver producto</a>
                    </div>
                    <div class="position-absolute w-100 h-100 px-1 py-2">
                        <div class="w-100 h-100 lazyload"
                        data-original="{{asset('img/marca/populares/Producto690_330.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="pop w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="d-flex flex-column justify-content-between">
                        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">ver producto</a>
                    </div>
                    <div class="position-absolute w-100 h-100 px-1 py-2">
                        <div class="w-100 h-100 lazyload"
                        data-original="{{asset('img/marca/populares/Producto690_330.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="pop w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="d-flex flex-column justify-content-between">
                        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">ver producto</a>
                    </div>
                    <div class="position-absolute w-100 h-100 px-1 py-2">
                        <div class="w-100 h-100 lazyload"
                        data-original="{{asset('img/marca/populares/Producto690_330.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="pop w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="d-flex flex-column justify-content-between">
                        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">ver producto</a>
                    </div>
                    <div class="position-absolute w-100 h-100 px-1 py-2">
                        <div class="w-100 h-100 lazyload"
                        data-original="{{asset('img/marca/populares/Producto690_330.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="pop w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="d-flex flex-column justify-content-between">
                        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">ver producto</a>
                    </div>
                    <div class="position-absolute w-100 h-100 px-1 py-2">
                        <div class="w-100 h-100 lazyload"
                        data-original="{{asset('img/marca/populares/Producto690_330.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="pop w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="d-flex flex-column justify-content-between">
                        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">ver producto</a>
                    </div>
                    <div class="position-absolute w-100 h-100 px-1 py-2">
                        <div class="w-100 h-100 lazyload"
                        data-original="{{asset('img/marca/populares/Producto690_330.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</div>