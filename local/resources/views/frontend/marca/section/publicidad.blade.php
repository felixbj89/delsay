<div class="col-12 col-sm-11 mx-auto">
    <div class="row no-gutters h-100 px-2">
        <div class="col-sm-12 col-md-7">
            <div id="publibanner" class="swiper-container"><!-- Swiper -->
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img data-src="{{ asset('img/marca/publicidad/publicidad1.jpg') }}" class="swiper-lazy">
                        <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                    </div>
                    <div class="swiper-slide">
                        <img data-src="{{ asset('img/marca/publicidad/publicidad2.jpg') }}" class="swiper-lazy">
                        <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 d-flex justify-content-center align-items-center">
            <div class="row m-0">
                <div class="col-12 px-3 py-3 py-sm-0">
                    <p>La colección de Counter Climate de Converse ha sido infundido con un diseño meticuloso, materiales de primera calidad, y una confección duradera para mantenerte cómodo, y haciendo lo que quiereshacer,sin importar el clima.</p>
                </div>
                <div class="col-12 px-3 pb-3 pb-sm-0">
                    <a href="" class="d-block text-uppercase text-center font-weight-normal btn-marcalight mx-auto">ver producto</a>
                </div>
            </div>
        </div>
    </div>
</div>