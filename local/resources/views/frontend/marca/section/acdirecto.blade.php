<div class="col-sm-12 col-md d-flex justify-content-center align-items-center">
    <div class="d-flex flex-column justify-content-between">
        <h4 class="d-inline-block align-self-start font-weight-bold text-under">PARA MUJER</h3>
        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">COMPRAR AHORA</a>
    </div>
    <div class="position-absolute w-100 h-100 px-1 py-2">
        <div class="w-100 h-100 lazyload"
        data-original="{{asset('img/marca/acdirectos/ac_mujeres.jpg')}}" 
        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
        </div>
    </div>
</div>
<div class="col-sm-12 col-md d-flex justify-content-center align-items-center">
    <div class="d-flex flex-column justify-content-between">    
        <h4 class="d-inline-block align-self-start font-weight-bold text-under">PARA HOMBRE</h3>
        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">COMPRAR AHORA</a>
    </div>
    <div class="position-absolute w-100 h-100 px-1 py-2">
        <div class="w-100 h-100 lazyload"
        data-original="{{asset('img/marca/acdirectos/ac_hombres.jpg')}}" 
        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
        </div>
    </div>
</div>
<div class="col-sm-12 col-md d-flex justify-content-center align-items-center position-relative">
    <div class="d-flex flex-column justify-content-between">      
        <h4 class="d-inline-block align-self-start font-weight-bold text-under">PARA NIÑA/O</h3>
        <a href="" class="text-uppercase text-center font-weight-normal btn-marca mx-auto">COMPRAR AHORA</a>
    </div>
    <div class="position-absolute w-100 h-100 px-1 py-2">
        <div class=" w-100 h-100 lazyload"
        data-original="{{asset('img/marca/acdirectos/ac_ninos.jpg')}}" 
        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
        </div>
    </div>
</div>