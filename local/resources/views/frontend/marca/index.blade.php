@extends("layout.master-brand")
@section("Mycss")
    <link href="{{ asset('css/marca/index.css') }}" rel="stylesheet">
@endsection
@section("header-page")
    @include('frontend.marca.componetes.header')
@endsection
@section("body-page")
    <section id="banner" class="row no-gutters">
        @include('frontend.marca.componetes.banneri')
    </section>
    <section id="acdirecto" class="row no-gutters">
        @include('frontend.marca.section.acdirecto')
    </section>
    <section id="populares" class="row no-gutters">
        @include('frontend.marca.section.populares')
    </section>
    <section id="publicidad" class="row no-gutters">
        @include('frontend.marca.section.publicidad')
    </section>
    <section id="masvendidos" class="row no-gutters">
        @include('frontend.marca.section.masvendidos')
    </section>
    <section id="redes" class="row no-gutters">
        @include('frontend.marca.section.redes')
    </section>
@endsection
@section("footer-page")
    @include('frontend.marca.componetes.footer')
@endsection
@section("Myscript")
    <script type="text/javascript" src="{{ asset('js/marca/index.js') }}"></script>
@endsection