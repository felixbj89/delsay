<div id="acercade" class="overlay row no-gutters h-100" style="display:none;background-image: url({{asset('img/background/fondopage.svg')}});">
    <div class="col-12 d-flex">
        <div  id="ctabs" class="w-100">
          <div class="row no-gutters">
            <div class="col-12 col-sm-11 mx-auto">
              <div class="py-2 pl-40px w-100">
                <nav class="mb-1">
                  <div class="nav nav-tabs row no-gutters" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link col text-center show active" id="nav-empresa-tab" data-toggle="tab" href="#nav-empresa" role="tab" aria-controls="nav-empresa" aria-selected="true">@lang('delsay.tabI')</a>
                    <a class="nav-item nav-link col text-center" id="nav-filosofia-tab" data-toggle="tab" href="#nav-filosofia" role="tab" aria-controls="nav-filosofia" aria-selected="false">@lang('delsay.tabII')</a>
                    <a class="nav-item nav-link col text-center" id="nav-marcas-tab" data-toggle="tab" href="#nav-marcas" role="tab" aria-controls="nav-marcas" aria-selected="fale">@lang('delsay.tabIII')</a>
                  </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                  <div class="tab-pane fade show active" id="nav-empresa" role="tabpanel" aria-labelledby="nav-empresa-tab">
                    <div class="row no-gutters">
                      <div class="col-12 col-sm-12 col-md-6">
                        <div class="h-100 w-100 p-2">
                          <div class="logacer h-100"></div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6 pt-3 d-flex align-items-center f">
                        <div class="w-100">
                          <p><strong>Misión</strong></p>
                          <p>Comercializar productos de la mejor calidad, con la finalidad de satisfacer las aspiraciones, necesidades, y exigencias de nuestro consumidor, promoviendo la confianza y seguridad en nuestra empresa.</p>
                          <p><strong>Visión</strong></p>
                          <p>Ser una empresa de excelencia con un crecimiento progresivo, que nos ayude a alcanzar niveles de comercialización importantes dentro del mercado venezolano y el Caribe, manteniendo un liderazgo y vanguardia de cada una de nuestras marcas, cumpliendo con los estándares de calidad e innovación, que le demuestren a nuestro consumidor final que nuestros productos van más allá del por qué lo utilizan.</p>
                        </div>
                      </div>
                    </div>                  
                  </div>
                  <div class="tab-pane fade" id="nav-filosofia" role="tabpanel" aria-labelledby="nav-filosofia-tab">
                    <div class="row no-gutters">
                      <div class="col-12 col-sm-12 col-md-6">
                        <div class="h-100 w-100 p-2">
                          <div class="logacer h-100"></div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6 pt-3 d-flex align-items-center f">
                        <div class="w-100">
                          <p><strong>¿Quiénes somos?</strong></p>
                          <p>Nuestras actividades iniciaron en Caracas – Venezuela, el 31 de julio de 1.996 teniendo como principal premisa la confianza en el país y su capacidad de desarrollo.</p>
                          <p>En poco tiempo, nos hemos convertido en líderes de la comercialización y distribución al mayor de textiles, calzados, y accesorios de algunas de las marcas más importantes y reconocidas a nivel mundial, tales como: Everlast®, Converse®, Joma®, Stiga®, Runic®, entre otras.</p>
                          <p>Hoy en día gracias a la confianza del consumidor final en nuestras marcas, hemos extendido el proyecto Del Say con la inauguración de tiendas concepto en las ciudades más importantes del país.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="nav-marcas" role="tabpanel" aria-labelledby="nav-marcas-tab">
                    <ul class="nav mb-1 row no-gutters mt-3" id="marcas-tab" role="tablist">
                      <li class="nav-item col">
                        <a class="nav-link-marcas active logo backgroundimg" style="background-image: url({{asset('img/marcas/everlast_marcas.svg')}});" id="marcas-everlast-tab" data-toggle="pill" href="#marcas-everlast" role="tab" aria-controls="marcas-everlast" aria-selected="true"></a>
                      </li>
                      <li class="nav-item col">
                        <a class="nav-link-marcas logo backgroundimg" style="background-image: url({{asset('img/marcas/converse_marcas.svg')}});" id="marcas-converse-tab" data-toggle="pill" href="#marcas-converse" role="tab" aria-controls="marcas-converse" aria-selected="false"></a>
                      </li>
                      <li class="nav-item col">
                        <a class="nav-link-marcas logo backgroundimg" style="background-image: url({{asset('img/marcas/kimbow_marcas.svg')}});" id="marcas-kimbow-tab" data-toggle="pill" href="#marcas-kimbow" role="tab" aria-controls="marcas-kimbow" aria-selected="false"></a>
                      </li>
                    </ul>
                    <div class="tab-content" id="marcas-tabContent">
                      <div class="tab-pane fade show active" id="marcas-everlast" role="tabpanel" aria-labelledby="marcas-everlast-tab">
                        <div class="row no-gutters">
                          <div class="col-12 align-self-start mt-2">
                            <div class="row no-gutters">        
                              <h3 class="col-12">La Grandeza está dentro</h3>
                              <div id="marcaeverlast" class="col-12 acercademarcas">
                                <p class="mb-0">Una marca activa en el diseño, comercialización de ropa y accesorios deportivos. Everlast nace en 1910 gracias al ingenio de Jacob Golomb.</p>
                                <p class="mb-0">Nuestros productos están diseñados para los deportistas que buscan calidad y vanguardia al momento de desarrollar actividades físicas.</p>
                                <p class="mb-0">Hoy en día contamos con una moderna línea textil (Dama / Caballero), accesorios y calzados. Desarrollamos desde implementos para Yoga, Fitness, Ciclismo, Spinning, Running, Levantamiento de pesas, Danza, Boxeo y actualmente somos la marca referencial de las Artes Marciales Mixtas (MMA), siendo este el deporte de mayor crecimiento en el mundo, entre otra disciplinas de contacto.</p>
                                <p class="mb-2">Somos una marca…fuerte, accesible, competitiva, innovadora, versatil y atemporal.</p>
                                <p class="mb-2">¡Construir campeones y desarrollar atletas te permite ser lo que tú quieres ser!</p>
                                <p class="mb-2">TECNOLOGÍA EN TEXTIL Y BOXEO EVERLAST</p>
                                <p class="mb-0">FEMENINO</p>
                                <p class="mb-0">EverCool</p>
                                <p class="mb-2">Los sistemas de ventilación y tejidos transpirables que componen la tecnología de rendimiento Evercool, son la fuente para la regulación de la temperatura corporal.</p>
                                <p class="mb-0">SEANLESS</p>
                                <p class="mb-2">Signiﬁca que no hay problemas de costuras . Nuestros artículos son sin cortes para moldear la pieza perfectamente a su cuerpo, las costuras no se interpone en tu camino puedes  correr un maratón, usar durante todo el día moverse o bailar  sin molestias.</p>
                                <p class="mb-0">VENTILACIÓN</p>
                                <p class="mb-2">Mantener la calma bajo presión, el ﬂujo de aire ayuda a regular su temperatura corporal para evitar sobrecalentamiento y seguir en movimiento fuerte.</p>
                                <p class="mb-0">BOLSILLOS CON CIERRE</p>
                                <p class="mb-2">Salir del gimnasio con todo lo que traía. Cómodo y seguro, el bolsillos con cierre garantiza que no importa lo fuerte de su entrenamiento, todo se mantiene resguardado.</p>
                                <p class="mb-0">RANURA BOLSILLO</p>
                                <p class="mb-2">La música es la motivación, llévala a donde vayas y guárdelos cómodamente.</p>
                                <p class="mb-0">MASCULINO</p>
                                <p class="mb-0">CottonFit</p>
                                <p class="mb-0">90% Algodón, Spandex 10%</p>
                                <p class="mb-2">La sensación natural y la fuerza adicional de esta mezcla de algodón spandex ofrece comodidad y apoyo durante su entrenamiento.</p>
                                <p class="mb-0">CottonFit (Carbonized)</p>
                                <p class="mb-0">90% Algodón, Spandex 10%.</p>
                                <p class="mb-2">Esta versión de nuestra forma de algodón texturizado, brinda una sensación de suavidad extra.</p>
                                <p class="mb-0">Supplex</p>
                                <p class="mb-0">90% Spandex de nylon del 10%.</p>
                                <p class="mb-2">Fabricado con un texturizado de nylon y ﬁbras naturales que proporciona la sensación de comodidad, manteniendo todas las cualidades antibacterianas y el color de nylon.</p>
                                <p class="mb-0">NylonFit</p>
                                <p class="mb-0">90% Spandex de nylon del 10%</p>
                                <p class="mb-2">Libera el sudor, por lo que siempre puede sentirse seco durante el entrenamiento. Cualidades antibacterianas y colore eterno, entrenamiento tras entrenamiento.<p>
                                <p class="mb-0">Hi-Tech Nylon Fit</p>
                                <p class="mb-0">90% Spandex de nylon del 10%</p>
                                <p class="mb-2">Nuestra versión de nylon con fuerza adicional de ajuste, es tecnológicamente diseñado para trabajar con compresión, permanece en su lugar, mejora la circulación, mejora su rendimiento.</p>
                                <p class="mb-0">Crepe</p>
                                <p class="mb-0">100% POLIESTER</p>
                                <p class="mb-0">El más suave tejido elástico que puede haber, ﬂexible como usted, va donde su cuerpo.</p>
                              </div>
                              <div class="d-flex flex-wrap nuestrastiendas">
                                <p class="m-2"><b>Caracas:</b><br/>
                                C.C. EL Líder -  Nivel California.<br/>
                                C.C. El Recreo – Nivel 5.<br/>
                                C.C. Sambil – Nivel Feria.</p>
                                <p class="m-2"><b>Los Teques:</b><br/>
                                C.C. Cascada Carrizal – Nivel 1.</p>
                                <p class="m-2"><b>San Cristobal:</b><br/>
                                C.C. Sambil – Nivel 2.</p>
                                <p class="m-2"><b>Barcelona:</b><br/>
                                C.C. Puente Real – Nivel PB.</p>
                                <p class="m-2"><b>Margarita:</b><br/>
                                C.C. Parque Costa Azul - Nivel PB.</p>
                                <p class="m-2"><b>Maracay:</b><br/>
                                C.C. Parque Aviadores Maracay - Nivel PB.</p>                            
                              </div>
                            </div>
                          </div>
                          <div class="col-12 align-self-end mt-2">

                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="marcas-converse" role="tabpanel" aria-labelledby="marcas-converse-tab">
                        <div class="row no-gutters">
                          <div class="col-12 align-self-start mt-2">
                            <div class="row no-gutters">        
                              <h3 class="col-12">¡Converse, va con todo!</h3>
                              <div id="marcaeverlast" class="col-12 acercademarcas">
                                <p class="mb-2">Más que zapatillas, Converse es la historia de leyendas, héroes y personajes innovadores, todos unidos por el amor al deporte. Cuando el Marqués M. Converse abrió las puertas de su fábrica, la Converse Rubber Shoe Company en Malden, Mass., en 1908, no tenía la más mínima idea que lo que estaba creando era un ícono americano.</p>
                                <p class="mb-2">A lo largo del siglo XX, Converse trajo consigo una constante creación de zapatillas deportivas de originales diseños, que incluyó la zapatilla que revolucionó el basketball y luego se convertiría en un favorito a nivel mundial, la Chuck Taylor® All Star®. </p>
                                <p class="mb-2">La historia de Converse – el verdadero espíritu del deporte americano – continua avanzando en los deportes y en el diario vivir aún en el siglo XXI.</p>
                                <p class="mb-2">En Venezuela Distribuidora Del Say 96, C.A. es la encargada de distribuir los productos originales de la marca con su amplia variedad de zapatillas, accesorios y textil.</p>
                                <p class="mb-2">Nuestras tiendas concepto ubícalas en:</p>
                              </div>
                              <div class="d-flex flex-wrap nuestrastiendas">
                                <p class="m-2"><b>Caracas:</b><br/>
                                C.C. Tolón Fashion Mall<br/>
                                C.C Líder<br/>
                                C.C. Sambil Caracas<br/>
                                C.C. San Ignacio</p>
                                <p class="m-2"><b>Maturín:</b><br/>
                                C.C. Cascada Maturín</p>
                                <p class="m-2"><b>Maracay:</b><br/>
                                C.C. Parque Aviadores.</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 align-self-end mt-2">

                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="marcas-kimbow" role="tabpanel" aria-labelledby="marcas-kimbow-tab">
                      <div class="row no-gutters">
                          <div class="col-12 align-self-start mt-2">
                            <div class="row no-gutters">        
                              <h3 class="col-12"></h3>
                              <div id="marcaeverlast" class="col-12 acercademarcas">
                                <p class="mb-2">Kimbow somos una empresa con sello venezolano que fabrica textiles y uniformes para disciplinas deportivas, para damas, caballeros y niños. Entre las que destacan franelas, franelillas, top, shorts, pantalones, licras, leggins, entre otras. Además contamos con accesorios para practicar yoga, running, fitness y workout a precios competitivos.</p>
                                <p class="mb-2">La ropa y accesorios deportivos Kimbow tienen acabados, diseños y materiales de primera calidad, destacándose para obtener un espacio en el mundo deportivo venezolano.</p>
                                <p class="mb-2">Nuestros intereses están centrados primeramente en abastecer la demanda del mercado local, en cuanto a artículos y accesorios deportivos con alta calidad y precios accesibles.</p>
                              </div>
                              <div class="d-flex flex-wrap nuestrastiendas">
                                <p class="m-2"><b>Caracas:</b><br/>
                                Sport Way<br/>
                                C.C. Galería Los Naranjos  -  Nivel PB.<br/>
                                C.C. Santa Fe – Nivel C1 Santa Fe.<br/>
                                Tiendas Beco a nivel nacional.<br/>
                                Tiendas de deportes a nivel nacional.</p>
                                <p class="m-2"><b>Barinas:</b><br/>
                                C.C. Cima - Nivel PB.</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 align-self-end mt-2">
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="ccontatad" class="row no-gutters">
              <br/>
              <div class="col-11 mx-auto">
                <div class="row no-gutters h-100">
                    <form id="formcontactoI" class="col-12 col-sm-10 col-md-10 col-lg-8 mx-auto align-self-center">
                        <div class="form-group row no-gutters mb-1">
                            <p class="col-12 text-center"><strong>¡CONÉCTATE CON NOSOTROS!</strong></p>
                        </div>
                        <div class="form-group row no-gutters mb-1">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="ventasCheckboxI" name="ventasCheckbox" value="1">
                                <label class="form-check-label" for="ventasCheckboxI">Ventas</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="mercadeoCheckboxI" name="mercadeoCheckbox" value="2">
                                <label class="form-check-label" for="mercadeoCheckboxI">Mercadeo</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="recursoshumanosCheckboxI" name="recursoshumanosCheckbox" value="3">
                                <label class="form-check-label" for="recursoshumanosCheckboxI">Recusos Humanos</label>
                            </div>            
                        </div>
                        <div class="form-group row no-gutters mb-1">
                            <div class="col-12 col-sm-6 pl-0 pr-sm-1">
                                <div class="position-relative w-100">
                                    <input class="form-control form-control-sm" type="text" id="nombreI" name="nombre" placeholder="nombre y apellido">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                                <div class="position-relative w-100">
                                    <input class="form-control form-control-sm my-1" type="email" id="emailI" name="email" placeholder="e-mail">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                                <div class="position-relative w-100">
                                    <input class="form-control form-control-sm" type="text" id="telefonoI" name="telefono" placeholder="telefono">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>                                
                            </div>
                            <div class="col-12 col-sm-6 pl-0 pl-sm-1">
                              <div class="position-relative w-100">
                                  <input class="form-control form-control-sm mt-1 mt-sm-0" type="text" id="asuntoI" name="asunto" placeholder="asunto">
                                  <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                  <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                              </div>
                              <div class="position-relative w-100">
                                  <textarea class="form-control form-control-sm mt-1" rows="2" id="mensajeI" name="mensaje" placeholder="mensaje"></textarea>
                                  <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                  <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                              </div>
                            </div>
                        </div>
                        <div class="form-group row no-gutters mb-1 d-flex flex-row-reverse">
                        <input id="su_contactI" class="form-control-sm" type="button" value="enviar" disabled>
                        </div>
                    </form>
                </div>
                <!-- ENVIANDO -->
                <div id="sendingI" class="row">
                    <div class="col-11 self-center alert alert-dismissible" role="alert">
                        <div class="row h100">
                            <div class="col-12">
                                <div class="logobanner self-center" style="background-image: url( {{asset('img/logos/logodelsayII.svg')}} );"></div>
                                <button id="dismisI" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button> 
                            </div>
                            <div class="col-12 align-self-center evn">
                                <div class="row">
                                    <div class="col-12 fa-2x text-center">
                                        <i class="fas fa-spinner fa-pulse"></i>
                                    </div>
                                    <div class="col-12 text-center">
                                        ENVIANDO...
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 align-self-center mensaje text-center">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>