<div id="noticia" class="overlay secnoticia row no-gutters h-100" style="display:none;">
    <div class="col-12 h-100">
        <div class="w-100 pl-40px h-100 row no-gutters d-flex">
            <div class="containernews col-12 col-sm-11 mx-auto align-self-center h-containernews">
                <div class="row no-gutters h-100 d-flex">
                    <div class="col-md-12 col-lg-11 mx-auto h-containernews align-self-center">
                        <button id="close1" class="p-0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve"><g><g id="cross"><g><polygon points="612,36.004 576.521,0.603 306,270.608 35.478,0.603 0,36.004 270.522,306.011 0,575.997 35.478,611.397      306,341.411 576.521,611.397 612,575.997 341.459,306.011" fill="#000000"/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></button>
                        <div class="row h-100">
                            <div id="carouselnews" class="col-sm-12 col-md-6 d-flex">
                                <div id="newscarousel" class="carousel slide align-self-center" data-ride="carousel" data-interval="2500">
                                    <ol class="carousel-indicators">
                                        <!--li data-target="#newscarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#newscarousel" data-slide-to="1"></li>
                                        <li data-target="#newscarousel" data-slide-to="2"></li-->
                                    </ol>
                                    <div class="carousel-inner w-100 h-100">
                                        <!--div class="w-100 h-100 carousel-item active">
                                            <img class="d-block mx-auto" src="{{asset('img/noticias/noticias8.jpg')}}" alt="First slide">
                                        </div>
                                        <div class="w-100 h-100 carousel-item">
                                            <img class="d-block mx-auto" src="{{asset('img/noticias/noticias8.jpg')}}" alt="Second slide">
                                        </div>
                                        <div class="w-100 h-100 carousel-item">
                                            <img class="d-block mx-auto" src="{{asset('img/noticias/noticias8.jpg')}}" alt="Third slide">
                                        </div-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="row no-gutters h-100 ppnoticia">
                                    <div class="col-12"> 
                                        <div class="w-100 h-100 px-2"> 
                                            <div class="row no-gutters h-100">
                                                <div class="col-12 d-flex align-items-center">
                                                    <div id="headernews" class="row no-gutters w-100">
                                                        <div id="titletitularnews" class="col-12">
                                                            <p class="titulartext">2018 COPA MUNDIAL DE LA FIFA</p>
                                                        </div>
                                                        <div class="col-12 mt-1">
                                                            <h5 class="titulartext">CUANDO LOS DEBUTANTES SON SENSACIÓNCUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                                                        </div>
                                                        <div class="col-12 mt-1">
                                                            <p class="m-0">(FIFA.com) 18 abr. 2018</p>
                                                        </div>    
                                                    </div>    
                                                </div>    
                                                <div id="newspaper" class="col-12 hvh-50">
                                                    <p>Islandia y Panamá debutan en el Mundial en Rusia 2018 Empezarán ante dos rivales experimentados, Argentina y Bélgica Repasamos algunos de los estrenos más memorables En junio, dos países sentirán por primera vez en su historia esa embriagadora mezcla de optimismo, entusiasmo e inquietud que implica animar a sus selecciones en una Copa Mundial de la FIFA™. Son las de Islandia y Panamá, dos conjuntos que darán sus primeros pasos en la gran cita del deporte rey frente a Argentina y Bélgica, respectivamente.</p>
                                                    <p>Les esperan pues dos huesos duros de roer en sus primeros encuentros, por lo que ambos sin duda examinarán atentamente la historia mundialista para tratar de inspirarse con las actuaciones de algunos de los debutantes más memorables. FIFA.com se ha sumergido enlos archivos para recordar a los equipos primerizos que mayor sensación causaron.</p>
                                                    <p>Islandia y Panamá debutan en el Mundial en Rusia 2018 Empezarán ante dos rivales experimentados, Argentina y Bélgica Repasamos algunos de los estrenos más memorables En junio, dos países sentirán por primera vez en su historia esa embriagadora mezcla de optimismo, entusiasmo e inquietud que implica animar a sus selecciones en una Copa Mundial de la FIFA™. Son las de Islandia y Panamá, dos conjuntos que darán sus primeros pasos en la gran cita del deporte rey frente a Argentina y Bélgica, respectivamente.</p>
                                                    <p>Les esperan pues dos huesos duros de roer en sus primeros encuentros, por lo que ambos sin duda examinarán atentamente la historia mundialista para tratar de inspirarse con las actuaciones de algunos de los debutantes más memorables. FIFA.com se ha sumergido enlos archivos para recordar a los equipos primerizos que mayor sensación causaron.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="w-100 px-2 d-flex">  
                                            <button type="button" class="pb-2 btn-delsaynews btn-sm d-flex align-items-center" data-toggle="popover-x" data-target="#myPopover10a" data-placement="top top-left" ><!-- data-trigger="focus" -->
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    width="26.627px" height="34px" viewBox="0 0 26.627 34" enable-background="new 0 0 26.627 34" xml:space="preserve">
                                                    <path fill="#B21E28" d="M18.362,23.396c-0.253,0.254-0.472,0.533-0.66,0.828l-8.366-5.08c0.221-0.553,0.347-1.154,0.347-1.787
                                                    c0-0.631-0.126-1.234-0.346-1.787l8.367-5.039c0.859,1.35,2.366,2.248,4.082,2.248c2.67,0,4.841-2.172,4.841-4.842
                                                    c0-2.668-2.171-4.84-4.841-4.84s-4.842,2.172-4.842,4.84c0,0.609,0.118,1.189,0.323,1.727l-8.379,5.045
                                                    c-0.865-1.318-2.355-2.193-4.048-2.193C2.171,12.516,0,14.688,0,17.357s2.171,4.842,4.841,4.842c1.692,0,3.182-0.873,4.048-2.193
                                                    l8.377,5.086c-0.209,0.545-0.321,1.127-0.321,1.729c0,1.293,0.504,2.508,1.418,3.424c0.944,0.943,2.185,1.414,3.424,1.414
                                                    s2.479-0.471,3.423-1.414c0.914-0.916,1.418-2.131,1.418-3.424s-0.504-2.51-1.418-3.424C23.321,21.508,20.25,21.508,18.362,23.396z
                                                    M21.786,4.064c2.136,0,3.873,1.738,3.873,3.873c0,2.137-1.737,3.875-3.873,3.875s-3.874-1.738-3.874-3.875
                                                    C17.912,5.803,19.65,4.064,21.786,4.064z M4.841,21.23c-2.136,0-3.873-1.736-3.873-3.873c0-2.135,1.737-3.873,3.873-3.873
                                                    s3.874,1.738,3.874,3.873C8.715,19.494,6.977,21.23,4.841,21.23z M24.524,29.559c-1.51,1.51-3.967,1.51-5.478,0
                                                    c-0.73-0.732-1.135-1.705-1.135-2.738c0-1.035,0.404-2.008,1.135-2.738c0.756-0.756,1.747-1.133,2.739-1.133
                                                    s1.983,0.377,2.738,1.133c0.731,0.73,1.135,1.703,1.135,2.738C25.659,27.854,25.256,28.826,24.524,29.559z"/>
                                                </svg>&nbsp;Compartir
                                            </button>
                                            <a id="viewimg" class="pb-2 ml-2 btn-delsaynews btn-sm d-flex align-items-center d-sm-block d-md-none" href="#">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    width="26.627px" height="34px" viewBox="0 0 26.627 34" enable-background="new 0 0 26.627 34" xml:space="preserve">
                                                    <path fill="#B21E28" d="M18.362,23.396c-0.253,0.254-0.472,0.533-0.66,0.828l-8.366-5.08c0.221-0.553,0.347-1.154,0.347-1.787
                                                    c0-0.631-0.126-1.234-0.346-1.787l8.367-5.039c0.859,1.35,2.366,2.248,4.082,2.248c2.67,0,4.841-2.172,4.841-4.842
                                                    c0-2.668-2.171-4.84-4.841-4.84s-4.842,2.172-4.842,4.84c0,0.609,0.118,1.189,0.323,1.727l-8.379,5.045
                                                    c-0.865-1.318-2.355-2.193-4.048-2.193C2.171,12.516,0,14.688,0,17.357s2.171,4.842,4.841,4.842c1.692,0,3.182-0.873,4.048-2.193
                                                    l8.377,5.086c-0.209,0.545-0.321,1.127-0.321,1.729c0,1.293,0.504,2.508,1.418,3.424c0.944,0.943,2.185,1.414,3.424,1.414
                                                    s2.479-0.471,3.423-1.414c0.914-0.916,1.418-2.131,1.418-3.424s-0.504-2.51-1.418-3.424C23.321,21.508,20.25,21.508,18.362,23.396z
                                                    M21.786,4.064c2.136,0,3.873,1.738,3.873,3.873c0,2.137-1.737,3.875-3.873,3.875s-3.874-1.738-3.874-3.875
                                                    C17.912,5.803,19.65,4.064,21.786,4.064z M4.841,21.23c-2.136,0-3.873-1.736-3.873-3.873c0-2.135,1.737-3.873,3.873-3.873
                                                    s3.874,1.738,3.874,3.873C8.715,19.494,6.977,21.23,4.841,21.23z M24.524,29.559c-1.51,1.51-3.967,1.51-5.478,0
                                                    c-0.73-0.732-1.135-1.705-1.135-2.738c0-1.035,0.404-2.008,1.135-2.738c0.756-0.756,1.747-1.133,2.739-1.133
                                                    s1.983,0.377,2.738,1.133c0.731,0.73,1.135,1.703,1.135,2.738C25.659,27.854,25.256,28.826,24.524,29.559z"/>
                                                </svg>&nbsp;Ver imagen
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PopoverX content -->
<div id="myPopover10a" class="popover popover-x popover-default">
    <div class="arrow"></div>
    <h5 class="popover-header popover-title"><span id="closepopover" class="close pull-right ml-3" data-dismiss="popover-x">&times;</span>Comparte en:</h5>
    <div class="popover-footer row no-gutters social">
        <div class="col">
            <a target="_blank" href="{{"https://www.facebook.com/sharer/sharer.php?u=".urlencode(asset('/'))}}" title="Compartir en Facebook" class="sharenews d-flex justify-content-center share">
                <svg style="height:45px;width:45px;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="42.631px" height="49.125px" viewBox="0 0 42.631 49.125" enable-background="new 0 0 42.631 49.125" xml:space="preserve"><g><g><g><path fill="#000" d="M26.114,15.603l-2.487-0.004c-2.8,0-4.604,1.855-4.604,4.73v2.176h-2.506c-0.214,0-0.39,0.176-0.39,0.391 v3.156c0,0.215,0.176,0.391,0.39,0.391h2.506v7.965c0,0.215,0.173,0.393,0.392,0.393h3.265c0.216,0,0.393-0.178,0.393-0.393 v-7.965h2.926c0.218,0,0.393-0.176,0.393-0.391l0.002-3.156c0-0.105-0.042-0.203-0.12-0.277 c-0.067-0.074-0.168-0.115-0.274-0.115h-2.926v-1.85c0-0.885,0.209-1.336,1.365-1.336h1.682c0.212,0,0.389-0.176,0.389-0.391 v-2.932C26.506,15.778,26.33,15.603,26.114,15.603z"/></g></g><g><path fill="#000" d="M21.316,45.2c-11.028,0-20-8.973-20-20s8.972-20,20-20s20,8.973,20,20S32.344,45.2,21.316,45.2z M21.316,6.532c-10.292,0-18.667,8.375-18.667,18.668s8.375,18.668,18.667,18.668c10.293,0,18.667-8.375,18.667-18.668 S31.609,6.532,21.316,6.532z"/></g></g></svg>
            </a>
        </div>
        <div class="col">
            <a target="_blank" href="{{"https://twitter.com/home?status=".urlencode(asset('/'))}}"  title="Compartir en Twitter" class="sharenews d-flex justify-content-center share">
                <svg style="height:40px;width:45px;padding-top: 5px;" enable-background="new 0 0 32 32" version="1.1" viewBox="0 0 32 32" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="OUTLINE_copy"><path d="M18.014,9.564c-1.439,0.523-2.348,1.874-2.244,3.351l0.035,0.57l-0.576-0.07   c-2.094-0.268-3.925-1.175-5.479-2.7L8.99,9.959l-0.196,0.559c-0.414,1.245-0.149,2.56,0.714,3.445   c0.46,0.489,0.356,0.559-0.437,0.268c-0.276-0.093-0.518-0.163-0.541-0.128c-0.08,0.082,0.196,1.14,0.414,1.559   c0.299,0.582,0.909,1.152,1.577,1.49l0.564,0.268l-0.667,0.012c-0.644,0-0.667,0.012-0.598,0.257   c0.23,0.756,1.139,1.559,2.152,1.909l0.714,0.244l-0.621,0.372c-0.921,0.536-2.003,0.838-3.085,0.861   c-0.519,0.012-0.944,0.058-0.944,0.093c0,0.116,1.405,0.767,2.221,1.024c2.451,0.756,5.364,0.43,7.551-0.861   c1.554-0.92,3.107-2.746,3.833-4.516c0.392-0.942,0.783-2.665,0.783-3.49c0-0.536,0.035-0.605,0.679-1.245   c0.38-0.372,0.737-0.779,0.806-0.896c0.115-0.221,0.103-0.221-0.483-0.024c-0.978,0.349-1.117,0.303-0.633-0.221   c0.356-0.372,0.783-1.047,0.783-1.245c0-0.035-0.172,0.023-0.369,0.128c-0.207,0.116-0.667,0.291-1.013,0.395l-0.621,0.198   l-0.564-0.385c-0.311-0.209-0.748-0.442-0.978-0.512C19.442,9.355,18.544,9.378,18.014,9.564z" fill="#000000"/><g><g id="Shopping_10_117_"><g><g><g><g><path d="M16,1c8.271,0,15,6.729,15,15s-6.729,15-15,15S1,24.271,1,16S7.729,1,16,1 M16,0 C7.163,0,0,7.164,0,16s7.163,16,16,16s16-7.164,16-16S24.837,0,16,0L16,0z" fill="#000000"/></g></g></g></g></g></g></g></svg>                
            </a>
            <!--a target="_blank" href="#"  title="Compartir en Instagram" class="sharenews d-flex justify-content-center share">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="42.631px" height="49.125px" viewBox="0 0 42.631 49.125" enable-background="new 0 0 42.631 49.125" xml:space="preserve"><g><g><g><g><g><path fill="#000" d="M25.23,16.474h-7.199c-3.313,0-6,2.688-6,6v7.201c0,3.313,2.688,6,6,6h7.199c3.314,0,6-2.688,6-6 v-7.201C31.23,19.161,28.544,16.474,25.23,16.474z M29.431,29.675c0,2.314-1.885,4.201-4.201,4.201h-7.199 c-2.316,0-4.201-1.887-4.201-4.201v-7.201c0-2.314,1.884-4.199,4.201-4.199h7.199c2.316,0,4.201,1.885,4.201,4.199V29.675z"/></g></g></g><g><g><g><path fill="#000" d="M21.63,21.274c-2.65,0-4.8,2.148-4.8,4.801s2.149,4.801,4.8,4.801s4.801-2.148,4.801-4.801 S24.281,21.274,21.63,21.274z M21.63,29.075c-1.652,0-3-1.348-3-3c0-1.656,1.348-3,3-3c1.654,0,3,1.344,3,3 C24.63,27.728,23.285,29.075,21.63,29.075z"/></g></g></g><g><g><g><circle fill="#000" cx="26.791" cy="20.915" r="0.641"/></g></g></g></g><g><path fill="#000" d="M21.63,46.075c-11.027,0-20-8.973-20-20s8.973-20,20-20c11.028,0,20,8.973,20,20 S32.659,46.075,21.63,46.075z M21.63,7.407c-10.292,0-18.666,8.375-18.666,18.668s8.374,18.668,18.666,18.668 c10.293,0,18.668-8.375,18.668-18.668S31.923,7.407,21.63,7.407z"/></g></g></svg>
            </a-->
        </div>
    </div>
</div>
<!-- 
    localforge cargar todas los noticias y traer a la modal 
    la indicada de localforge.
-->