@extends("layout.master")
@section("Mycss")
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <!-- lightbox -->
    <link href="{{ asset('plugins/lightbox/css/lightbox.min.css') }}" rel="stylesheet">
@endsection
@section("body-page")
    <!-- Swiper -->
    <div id="swiperdelsay"class="swiper-container w-100 h-100">
        <div class="swiper-wrapper">
            <section id="banner" class="row no-gutters panel swiper-slide">
                @include('frontend.index.banner')
            </section>
            <section id="productos" class="row no-gutters h-100 panel swiper-slide bg-white">
                @include('frontend.index.productos')
            </section>
            <section id="populares" class="row no-gutters panel swiper-slide bg-white">
                @include('frontend.index.populares')
            </section>
            <section id="noticias" class="row no-gutters h-100 panel swiper-slide bg-white">
                @include('frontend.index.noticias')
            </section>
            <section id="tiendas" class="row no-gutters h-100 panel swiper-slide bg-white">
                @include('frontend.index.tiendas')
            </section>
            <section id="contacto" class="row no-gutters h-100 panel swiper-slide bg-white" style="background-image: url({{asset('img/background/fondopage.svg')}});">
                @include('frontend.index.contacto')
            </section>
            <footer id="footer" class="row no-gutters swiper-slide w-100" style="background-image: url({{asset('img/background/fondopage.svg')}});">
                @include('component.footer')
            </footer>
        </div>
    </div>
    @include('frontend.nomodal.noticias')
    @include('frontend.nomodal.acercade')
    @include('frontend.nomodal.ubicacion')
    @include('frontend.modal.suscribe')
@endsection
@section("Myscript")
    <script src="{{ asset('plugins/velocity/velocity.min.js')}}"></script>
    <script src="{{ asset('plugins/velocity/velocity.ui.min.js')}}"></script>
    <script src="{{ asset('plugins/masked/jquery.maskedinput.min.js')}}"></script>
    <script src="{{ asset('js/index/index.js') }}"></script>
    <!-- localforage -->
    <script type="text/javascript" src="{{ asset('plugins/localforage/dist/localforage.min.js') }}"></script>
    <!-- lightbox -->
    <script type="text/javascript" src="{{ asset('plugins/lightbox/js/lightbox.min.js') }}"></script>  
@endsection