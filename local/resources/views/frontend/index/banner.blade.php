<div id="prinbanner" class="col-12 swiper-container mx-auto">
    <div class="swiper-wrapper">
        <div class="swiper-slide swiper-lazy d-flex" data-background="{{asset('img/background/slider_1.jpg')}}">
            <div class="swiper-lazy-preloader"></div>
            <div id="eslogan_0" class="eslogan h-75 w-100 pl-40px align-self-center" style="display:none;">
                <div class="d-flex h-100 w-100 row no-gutters justify-content-center">
                    <div class="col-12 col-sm-11 col-md-9 align-self-center">
                        <div class="p-0 d-inline-block mw-100">
                            <h1 class="text-bn font-weight-normal mb-0" style="font-size: 3rem;line-height: 2.5rem;"><span style="color: rgb(255, 255, 255);"><span style="font-family: &quot;Helvetica&quot;;">LA</span></span></h1>
                            <h1 class="text-bn font-weight-normal mb-0 pr-2 pr-md-4 pr-lg-4" style="font-size: 4rem;line-height: 3rem;"><span style="color: rgb(255, 255, 255);"><span style="font-family: &quot;Helvetica&quot;;">GRANDEZA</span></span></h1>
                            <h1 class="text-bn font-weight-normal mb-0" style="font-size: 3rem;line-height: 2.5rem;text-align: right;z-index: 10;position: relative;"><span style="color: rgb(255, 255, 255);"><span style="font-family: &quot;Helvetica&quot;;">ESTÁ</span></span></h1>
                            <h1 class="text-bn font-weight-normal mb-0" style="font-size: 4.5rem;line-height: 3.5rem;"><span class="py-0 pl-5 pr-2" style="color: rgb(0, 0, 0);background-image: url('img/background/SliderEverlast.svg');background-size: 100%;background-repeat: no-repeat;background-position: bottom left;"><span style="font-family: &quot;Helvetica&quot;;">DENTRO</span></span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide swiper-lazy d-flex" data-background="{{asset('img/background/slider_2.jpg')}}">
            <div class="swiper-lazy-preloader"></div>
            <div id="eslogan_1" class="eslogan h-75 w-100 pl-40px align-self-center" style="display:none;">
                <div class="d-flex h-100 w-100 row no-gutters justify-content-center">
                    <div class="col-sm-11 col-md-9  align-self-center">
                        <div class="p-0 d-inline-block mw-100">
                            <h1 class="text-bn font-weight-normal mb-0" style="font-size: 4.5rem;line-height: 4.9rem;"><span style="color: rgb(255, 255, 255);background-image: url('img/background/SiliderConverse.svg');background-size: 100% 60%;background-repeat: no-repeat;background-position: bottom center;">¡Converse,</span></h1>
                            <h1 class="text-bn pl-4 pl-md-5 font-weight-normal mb-0" style="font-size: 4.5rem;line-height: 4rem;"><span style="color: rgb(255, 255, 255);background-image: url('img/background/SiliderConverse.svg');background-size: 100% 60%;background-repeat: no-repeat;background-position: bottom center;">va con todo!</span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide swiper-lazy d-flex" data-background="{{asset('img/background/slider_3.jpg')}}">
            <div class="swiper-lazy-preloader"></div>
            <div id="eslogan_2" class="eslogan h-75 w-100 pl-40px align-self-center" style="display:none;">
                <div class="d-flex h-100 w-100 row no-gutters justify-content-center">
                    <div class="col-sm-11 col-md-9  align-self-center">
                        <div class="brn p-0 d-inline-block mw-100 float-right">
                            <h1 class="text-bn font-weight-bold mb-0" style="font-size: 4.5rem;line-height: 4rem;"><span style="color: rgb(255, 255, 255); font-family: &quot;Helvetica&quot;;">VEMOS</span></h1>
                            <h1 class="text-bn font-weight-bold mb-0 pl-2 pl-md-5" style="font-size: 4.5rem;line-height: 4rem;"><span style="color: rgb(255, 255, 0); font-family: &quot;Helvetica&quot;;">EL MUNDO</span></h1>
                            <h1 class="text-bn font-weight-bold mb-0" style="font-size: 3rem;line-height: 3.2rem;"><span style="color: rgb(255, 255, 255); font-family: &quot;Helvetica&quot;;">de </span><span style="color: rgb(0, 0, 0); font-family: &quot;Helvetica&quot;;background-image: url('img/background/SliderKimbow2.svg');background-size: 100% 100%;background-repeat: no-repeat;background-position: bottom center;">otra forma</span><span style="font-family: &quot;Helvetica&quot;;"></span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-pagination"></div>
</div>