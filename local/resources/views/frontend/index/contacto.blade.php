<div class="row no-gutters h-100 w-100">
    <div class="col-12">
        <div class="w-100 h-100 pl-40px">
            <div class="row no-gutters h-100">
                <div class="col-11 mx-auto">

                </div>
                <div class="col-11 mx-auto">
                    <div class="row no-gutters h-100">
                        <form id="formcontacto" class="col-12 col-sm-10 col-md-10 col-lg-8 mx-auto align-self-center">
                            <div class="form-group row no-gutters mb-1">
                                <h5 class="col-12 text-center"><strong>¡CONÉCTATE CON NOSOTROS!</strong></h5>
                            </div>
                            <div class="form-group row no-gutters mb-1">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="ventasCheckbox" name="ventasCheckbox" value="1">
                                    <label class="form-check-label" for="ventasCheckbox">Ventas</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="mercadeoCheckbox" name="mercadeoCheckbox" value="2">
                                    <label class="form-check-label" for="mercadeoCheckbox">Mercadeo</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="recursoshumanosCheckbox" name="recursoshumanosCheckbox" value="3">
                                    <label class="form-check-label" for="recursoshumanosCheckbox">Recusos Humanos</label>
                                </div>
                            </div>
                            <div class="form-group row no-gutters mb-1">
                                <div class="col-12 col-sm-6 pl-0 pr-sm-1">
                                    <div class="position-relative w-100">
                                        <input class="form-control form-control-sm" type="text" id="nombre" name="nombre" placeholder="nombre y apellido">
                                        <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                        <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                    </div>
                                    <div class="position-relative w-100">
                                        <input class="form-control form-control-sm my-1" type="email" id="email" name="email" placeholder="e-mail">
                                        <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                        <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                    </div>
                                    <div class="position-relative w-100">
                                        <input class="form-control form-control-sm" type="text" id="telefono" name="telefono" placeholder="telefono">
                                        <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                        <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 pl-0 pl-sm-1">
                                    <div class="position-relative w-100">
                                        <input class="form-control form-control-sm mt-1 mt-sm-0" type="text" id="asunto" name="asunto" placeholder="asunto">
                                        <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                        <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                    </div>
                                    <div class="position-relative w-100">
                                        <textarea class="form-control form-control-sm mt-1" rows="2" id="mensaje" name="mensaje" placeholder="mensaje"></textarea>
                                        <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                        <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row no-gutters mb-1 d-flex flex-row-reverse">
                                <input id="su_contact" class="form-control-sm" type="button" value="enviar" disabled>
                            </div>
                        </form>
                    </div>
                    <!-- ENVIANDO -->
                    <div id="sending" class="row">
                        <div class="col-11 self-center alert alert-dismissible" role="alert">
                            <div class="row h100">
                                <div class="col-12">
                                    <div class="logobanner self-center" style="background-image: url( {{asset('img/logos/logodelsayII.svg')}} );"></div>
                                    <button id="dismis" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button> 
                                </div>
                                <div class="col-12 align-self-center evn">
                                    <div class="row">
                                        <div class="col-12 fa-2x text-center">
                                            <i class="fas fa-spinner fa-pulse"></i>
                                        </div>
                                        <div class="col-12 text-center">
                                            ENVIANDO...
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 align-self-center mensaje text-center">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-11 mx-auto">

                </div>
            </div>
        </div>
    </div>
</div>