<div class="col-12 h-100 p-0">
    <div class="w-100 h-100">
        <div id="noticiasswiper" class="swiper-container h-100 w-100">
            <div class="swiper-wrapper d-flex">
                <div class="swiper-slide order-0 order-xl-1">
                    <!--Imagen Noticia 1-->
                    <!--div class="w-100 h-100 background" style="background-image: url({{asset('img/noticias/noticias1.jpg')}});"></div-->
                    <div class="w-100 h-100 background lazyload"
                        data-original="{{asset('img/noticias/converse_1.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                </div>
                <div class="swiper-slide order-1 order-xl-0 d-flex">
                <!--Noticia 1-->
                    <div id="news1" class="newslide row no-gutters align-self-center">
                        <div class="col-12 justify-content-center">
                            <div class="w-100 mb-1">
                                <!--h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5-->
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">2018 COPA MUNDIAL DE LA FIFA</p>
                        </div>
                        <div class="col-12">
                            <h5 class="mb-1 titular">CUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">(FIFA.com) 18 abr. 2018</p>
                        </div>
                    </div>
                    <!--a class="btn-plus open" href="Noticia1"></a-->
                    <a class="btn-tostore open" href="news1"></a>
                </div>
                <div class="swiper-slide order-2 d-flex">
                    <!--Noticia 2-->
                    <div id="news2" class="newslide row no-gutters align-self-center">
                        <div class="col-12 justify-content-center">
                            <div class="w-100 mb-1">
                                <!--h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5-->
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">2018 COPA MUNDIAL DE LA FIFA</p>
                        </div>
                        <div class="col-12">
                            <h5 class="mb-1 titular">CUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">(FIFA.com) 18 abr. 2018</p>
                        </div>
                    </div>
                    <!--a class="btn-plus open" href="Noticia2"></a-->
                    <a class="btn-tostore open" href="news2"></a>
                </div>
                <div class="swiper-slide order-3 order-xl-6 d-flex">
                    <!--Noticia 4-->
                    <div id="news4" class="newslide row no-gutters align-self-center">
                        <div class="col-12 justify-content-center">
                            <div class="w-100 mb-1">
                                <!--h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5-->
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">2018 COPA MUNDIAL DE LA FIFA</p>
                        </div>
                        <div class="col-12">
                            <h5 class="mb-1 titular">CUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">(FIFA.com) 18 abr. 2018</p>
                        </div>
                    </div>
                    <!--a class="btn-plus open" href="Noticia4"></a-->
                    <a class="btn-tostore open" href="news4"></a>
                </div>
                <div class="swiper-slide order-4 order-sm-5 d-flex">
                    <!--Noticia 3-->
                    <div id="news3" class="newslide row no-gutters align-self-center">
                        <div class="col-12 justify-content-center">
                            <div class="w-100 mb-1">
                                <!--h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5-->
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">2018 COPA MUNDIAL DE LA FIFA</p>
                        </div>
                        <div class="col-12">
                            <h5 class="mb-1 titular">CUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">(FIFA.com) 18 abr. 2018</p>
                        </div>
                    </div>
                    <!--a class="btn-plus open" href="Noticia3"></a-->
                    <a class="btn-tostore open" href="news3"></a>
                </div>
                <div class="swiper-slide order-5 order-sm-4 d-flex">
                    <!--Imagen Noticia 3-->
                    <!--div class="w-100 h-100 background" style="background-image: url({{asset('img/noticias/noticias3.jpg')}});"></div-->
                    <div class="w-100 h-100 background lazyload"
                        data-original="{{asset('img/noticias/everlast_1.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                </div>
                <div class="swiper-slide order-6 order-sm-7 order-xl-3 d-flex">
                    <!--Imagen Noticia 5-->
                    <!--div class="w-100 h-100 background" style="background-image: url({{asset('img/noticias/noticias5.jpg')}});"></div-->
                    <div class="w-100 h-100 background lazyload"
                        data-original="{{asset('img/noticias/noticias5.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                </div>
                <div class="swiper-slide order-7 order-sm-6 d-flex">
                    <!--Noticia 5-->
                    <div id="news5" class="newslide row no-gutters align-self-center">
                        <div class="col-12 justify-content-center">
                            <div class="w-100 mb-1">
                                <!--h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5-->
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">2018 COPA MUNDIAL DE LA FIFA</p>
                        </div>
                        <div class="col-12">
                            <h5 class="mb-1 titular">CUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">(FIFA.com) 18 abr. 2018</p>
                        </div>
                    </div>
                    <!--a class="btn-plus open" href="Noticia5"></a-->
                    <a class="btn-tostore open" href="news5"></a>
                </div>
                <div class="swiper-slide order-8 d-flex">
                    <!--Noticia 6-->
                    <div id="news6" class="newslide row no-gutters align-self-center">
                        <div class="col-12 justify-content-center">
                            <div class="w-100 mb-1">
                                <!--h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5-->
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">2018 COPA MUNDIAL DE LA FIFA</p>
                        </div>
                        <div class="col-12">
                            <h5 class="mb-1 titular">CUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">(FIFA.com) 18 abr. 2018</p>
                        </div>
                    </div>
                    <!-- class="btn-plus open" href="Noticia6"></a-->
                    <a class="btn-tostore open" href="news6"></a>
                </div>
                <div class="swiper-slide order-9 d-flex">
                    <!--Noticia 7-->
                    <div id="news7" class="newslide row no-gutters align-self-center">
                        <div class="col-12 justify-content-center">
                            <div class="w-100 mb-1">
                                <!--h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5-->
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">2018 COPA MUNDIAL DE LA FIFA</p>
                        </div>
                        <div class="col-12">
                            <h5 class="mb-1 titular">CUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">(FIFA.com) 18 abr. 2018</p>
                        </div>
                    </div>
                    <!--a class="btn-plus open" href="Noticia7"></a-->
                    <a class="btn-tostore open" href="news7"></a>
                </div>
                <div class="swiper-slide order-10 d-flex">
                    <!--Noticia 8-->
                    <div id="news8" class="newslide row no-gutters align-self-center">
                        <div class="col-12 justify-content-center">
                            <div class="w-100 mb-1">
                                <!--h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5>
                                <h5 class="d-inline mb-1"><span class="delsaytag">FUTBOL</span></h5-->
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">2018 COPA MUNDIAL DE LA FIFA</p>
                        </div>
                        <div class="col-12">
                            <h5 class="mb-1 titular">CUANDO LOS DEBUTANTES SON SENSACIÓN</h5>
                        </div>
                        <div class="col-12">
                            <p class="mb-1">(FIFA.com) 18 abr. 2018</p>
                        </div>
                    </div>
                    <!--a class="btn-plus open" href="Noticia8"></a-->
                    <a class="btn-tostore open" href="news8"></a>
                </div>
                <div class="swiper-slide order-11 d-flex">
                    <!--Imagen Noticia 8-->
                    <!--div class="w-100 h-100 background" style="background-image: url({{asset('img/noticias/noticias8.jpg')}});"></div-->
                    <div class="w-100 h-100 background lazyload"
                        data-original="{{asset('img/noticias/noticias8.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
