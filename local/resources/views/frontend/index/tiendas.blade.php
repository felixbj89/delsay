<div class="col-12">
    <div class="row no-gutters h-100">
        <div class="col-12 col-sm d-flex order-1 order-sm-0 justify-content-center justify-content-sm-start">
            <div class="pl-40px align-self-center">
                <select id="filterstore">
                    <option selected value="all">Todas</option>
                    <option value="everlast">Tiendas Everlast </option>
                    <option value="converse">Tiendas Converse</option>
                    <!--option value="kimbow">Tiendas Kimbow</option-->
                    <option value="multimarca">Tiendas Multimarca</option>
                </select>
            </div>
        </div>
        <div class="col-12 col-sm d-flex order-0 order-sm-1"><h3 class="mb-0 mx-auto align-self-center">@lang('tiendas.titulo_page')</h3></div>
        <div class="col-12 col-sm d-flex order-2 order-sm-2"></div>
    </div>
</div>
<div class="col-12">
    <div class="w-100 h-100 pl-40px">
        <div id="tiendasswiper" class="swiper-container">
            <div class="swiper-wrapper">
                <!-- tienda 1 -->
                <div data-filter="@lang('tiendas.filter_1')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_1')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_1')" data-address = "@lang('tiendas.direccion_1')" data-titulostore = "@lang('tiendas.titulo_1')" data-latitud = "10.4804162" data-longitud = "-66.8609289">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_1')</p>
                                    <p>@lang('tiendas.horario_1')</p>
                                    <p>@lang('tiendas.telefono_1')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_1'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 2 -->
                <div data-filter="@lang('tiendas.filter_2')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_2')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_2')" data-address = "@lang('tiendas.direccion_2')" data-titulostore = "@lang('tiendas.titulo_2')" data-latitud = "10.4859218" data-longitud = "-66.8211584">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_2')</p>
                                    <p>@lang('tiendas.horario_2')</p>
                                    <p>@lang('tiendas.telefono_2')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_2'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 3 -->
                <div data-filter="@lang('tiendas.filter_3')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_3')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_3')" data-address = "@lang('tiendas.direccion_3')" data-titulostore = "@lang('tiendas.titulo_3')" data-latitud = "10.4895618" data-longitud = "-66.8545019">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_3')</p>
                                    <p>@lang('tiendas.horario_3')</p>
                                    <p>@lang('tiendas.telefono_3')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_3'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 4 -->
                <div data-filter="@lang('tiendas.filter_4')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_4')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_4')" data-address = "@lang('tiendas.direccion_4')" data-titulostore = "@lang('tiendas.titulo_4')" data-latitud = "10.4978158" data-longitud = "-66.8567229">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_4')</p>
                                    <p>@lang('tiendas.horario_4')</p>
                                    <p>@lang('tiendas.telefono_4')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_4'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 5 -->
                <div data-filter="@lang('tiendas.filter_5')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_5')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_5')" data-address = "@lang('tiendas.direccion_5')" data-titulostore = "@lang('tiendas.titulo_5')" data-latitud = "9.7029316" data-longitud = "-63.1542429">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_5')</p>
                                    <p>@lang('tiendas.horario_5')</p>
                                    <p>@lang('tiendas.telefono_5')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_5'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 6 -->
                <div data-filter="@lang('tiendas.filter_6')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_6')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_6')" data-address = "@lang('tiendas.direccion_6')" data-titulostore = "@lang('tiendas.titulo_6')" data-latitud = "10.187082" data-longitud = "-67.5808958">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_6')</p>
                                    <p>@lang('tiendas.horario_6')</p>
                                    <p>@lang('tiendas.telefono_6')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_6'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 7 -->
                <div data-filter="@lang('tiendas.filter_7')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_6')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_7')" data-address = "@lang('tiendas.direccion_7')" data-titulostore = "@lang('tiendas.titulo_7')" data-latitud = "10.4860141" data-longitud = "-66.8212066">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_7')</p>
                                    <p>@lang('tiendas.horario_7')</p>
                                    <p>@lang('tiendas.telefono_7')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_7'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 8 -->
                <div data-filter="@lang('tiendas.filter_8')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_8')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_8')" data-address = "@lang('tiendas.direccion_8')" data-titulostore = "@lang('tiendas.titulo_8')" data-latitud = "10.4916753" data-longitud = "-66.8773735">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_8')</p>
                                    <p>@lang('tiendas.horario_8')</p>
                                    <p>@lang('tiendas.telefono_8')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_8'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 9 -->
                <div data-filter="@lang('tiendas.filter_9')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_9')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_9')" data-address = "@lang('tiendas.direccion_9')" data-titulostore = "@lang('tiendas.titulo_9')" data-latitud = "10.4895882" data-longitud = "-66.8545012">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_9')</p>
                                    <p>@lang('tiendas.horario_9')</p>
                                    <p>@lang('tiendas.telefono_9')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_9'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 10 -->
                <div data-filter="@lang('tiendas.filter_10')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_10')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_10')" data-address = "@lang('tiendas.direccion_10')" data-titulostore = "@lang('tiendas.titulo_10')" data-latitud = "10.3543877" data-longitud = "-67.0030753">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_10')</p>
                                    <p>@lang('tiendas.horario_10')</p>
                                    <p>@lang('tiendas.telefono_10')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_10'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 11 -->
                <div data-filter="@lang('tiendas.filter_11')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_11')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_11')" data-address = "@lang('tiendas.direccion_11')" data-titulostore = "@lang('tiendas.titulo_11')" data-latitud = "7.7932458" data-longitud = "-72.2349354">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_11')</p>
                                    <p>@lang('tiendas.horario_11')</p>
                                    <p>@lang('tiendas.telefono_11')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_11'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 12 -->
                <div data-filter="@lang('tiendas.filter_12')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_12')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_12')" data-address = "@lang('tiendas.direccion_12')" data-titulostore = "@lang('tiendas.titulo_12')" data-latitud = "10.1561705" data-longitud = "-64.6985835">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_12')</p>
                                    <p>@lang('tiendas.horario_12')</p>
                                    <p>@lang('tiendas.telefono_12')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_12'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 13 -->
                <div data-filter="@lang('tiendas.filter_13')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_13')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_13')" data-address = "@lang('tiendas.direccion_13')" data-titulostore = "@lang('tiendas.titulo_13')" data-latitud = "10.9909329" data-longitud = "-63.8264009">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_13')</p>
                                    <p>@lang('tiendas.horario_13')</p>
                                    <p>@lang('tiendas.telefono_13')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_13'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 14 -->
                <div data-filter="@lang('tiendas.filter_14')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_14')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_14')" data-address = "@lang('tiendas.direccion_14')" data-titulostore = "@lang('tiendas.titulo_14')" data-latitud = "10.1871134" data-longitud = "-67.5810032">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_14')</p>
                                    <p>@lang('tiendas.horario_14')</p>
                                    <p>@lang('tiendas.telefono_14')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_14'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 15 -->
                <div data-filter="@lang('tiendas.filter_15')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_15')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_15')" data-address = "@lang('tiendas.direccion_15')" data-titulostore = "@lang('tiendas.titulo_15')" data-latitud = "10.4401835" data-longitud = "-66.8362872">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_15')</p>
                                    <p>@lang('tiendas.horario_15')</p>
                                    <p>@lang('tiendas.telefono_15')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_15'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 16 -->
                <div data-filter="@lang('tiendas.filter_16')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_16')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_16')" data-address = "@lang('tiendas.direccion_16')" data-titulostore = "@lang('tiendas.titulo_16')" data-latitud = "10.4661785" data-longitud = "-66.8715135">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_16')</p>
                                    <p>@lang('tiendas.horario_16')</p>
                                    <p>@lang('tiendas.telefono_16')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_16'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
                <!-- tienda 17 -->
                <div data-filter="@lang('tiendas.filter_17')" class="swiper-slide slidestore">
                    <div class="row h-100 mx-0 d-flex">
                        <div class="px-4 col-12 col-sm-7 col-md-7 col-lg-7 col-xl-6 align-self-center">
                            <h5>@lang('tiendas.tienda_17')</h5>
                            <div class="media d-flex">
                                <img class="locatorstore order-sm-0 order-1 align-self-start" src="{{asset('img/icons/ubicacion_icon_ negro.svg')}}" data-urlgoogle = "@lang('tiendas.ubicacion_17')" data-address = "@lang('tiendas.direccion_17')" data-titulostore = "@lang('tiendas.titulo_17')" data-latitud = "8.6144556" data-longitud = "-70.2501296">
                                <div class="media-body order-sm-1 order-0">
                                    <p>@lang('tiendas.direccion_17')</p>
                                    <p>@lang('tiendas.horario_17')</p>
                                    <p>@lang('tiendas.telefono_17')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-6 backgroundimg lazyload"
                        data-original="{{asset('img/tiendas/'.\Lang::get('tiendas.imagen_17'))}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"></div>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div id="pagination" class="swiper-pagination"></div>
        </div>
    </div>
</div>