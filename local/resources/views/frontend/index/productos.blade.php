<div id="productosswiper" class="col-12 swiper-container mx-auto h-100">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="row no-gutters h-100">
                <div id="product_converse" class="col-12 backgroundimg lazyload"
                data-original="{{asset('img/background/productos_conv.jpg')}}" 
                style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"><!--CONVERSE--></div>
            </div>
            <div class="contproducts row no-gutters blureffect h-100"  data-blur="#product_converse">
                <div class="col-12 align-self-center backgroundimg isotipo" style="background-image: url({{asset('img/productos/isotipo_converse.svg')}});"></div>
                <div class="col-12 text-center align-self-center tostore px-15">
                    <div class="row no-gutters tienda">
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto">
                            <div class="logo backgroundimg mx-auto" style="background-image: url({{asset('img/productos/converse_productos_hover.svg')}});"></div>
                        </div>
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto parrafo">
                            <p>Más que zapatillas, Converse es la historia de leyendas, héroes y personajes innovadores, todos unidos por el amor al deporte. ¡Converse, va con todo!</p>
                        </div>
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto boton">
                            <a class="btn-tostore toContacproductos"  data-swiper="4" @if(Request::is('/')) href="converse" @else href="{{url('/')}}" @endif >@lang('delsay.tostore')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="row no-gutters h-100">
                <div id="product_everlast" class="col-12 backgroundimg lazyload"
                data-original="{{asset('img/background/productos_everlast.jpg')}}" 
                style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"><!--EVERLAST--></div>
            </div>
            <div class="contproducts row no-gutters blureffect h-100"  data-blur="#product_everlast">
                <div class="col-12 align-self-center backgroundimg isotipo" style="background-image: url({{asset('img/productos/isotipo_everlast.svg')}});"></div>
                <div class="col-12 text-center align-self-center tostore px-15">
                    <div class="row no-gutters tienda">
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto">
                            <div class="logo backgroundimg mx-auto" style="background-image: url({{asset('img/productos/everlast_productos_hover.svg')}});"></div>
                        </div>
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto parrafo">
                            <p>Desde 1910, marca activa en el diseño, comercialización de ropa y accesorios deportivos. "La Grandeza está dentro"</p>
                        </div>
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto boton">
                            <a class="btn-tostore toContacproductos" data-swiper="4" @if(Request::is('/')) href="everlast" @else href="{{url('/')}}" @endif >@lang('delsay.tostore')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="row no-gutters h-100">
                <div id="product_kimbow" class="col-12 backgroundimg lazyload"
                data-original="{{asset('img/background/productos_kimbow.jpg')}}" 
                style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"><!--KIMBOW--></div>
            </div>
            <div class="contproducts row no-gutters blureffect h-100"  data-blur="#product_kimbow">
                <div class="col-12 align-self-center backgroundimg isotipo" style="background-image: url({{asset('img/productos/isotipo_kimbow.svg')}});"></div>
                <div class="col-12 text-center align-self-center tostore px-15">
                    <div class="row no-gutters tienda">
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto">
                            <div class="logo backgroundimg mx-auto" style="background-image: url({{asset('img/productos/kimbow_productos_hover.svg')}});"></div>
                        </div>
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto parrafo">
                            <p>Marca con sello venezolano que fabrica textiles y uniformes para disciplinas deportivas, para damas, caballeros y niños.</p>
                        </div>
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto boton">
                            <a class="btn-tostore" href="https://tienda.mercadolibre.com.ve/kimbow" target="_blank">@lang('delsay.tostore')</a>
                        </div><!-- toContacproductos data-swiper="4" -->
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="row no-gutters h-100">
                <div id="product_milti" class="col-12 backgroundimg lazyload"
                data-original="{{asset('img/background/productos_multimarcas.jpg')}}" 
                style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});"><!--MULTIMARCAS--></div>
            </div>
            <div class="contproducts row no-gutters blureffect h-100"  data-blur="#product_milti">
                <div class="col-12 align-self-center backgroundimg isotipo" style="background-image: url({{asset('img/productos/multimarcas_productos.svg')}});"></div>
                <div class="col-12 text-center align-self-center tostore px-15">
                    <div class="row no-gutters tienda">
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto">
                            <div class="logo backgroundimg mx-auto" style="background-image: url({{asset('img/productos/multimarcas_productos_hover.svg')}});"></div>
                        </div>
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto parrafo">
                            <p>Las Tiendas multimarca  Sport Way y One Store de la distribuidora Delsay comercializan productos nacionales e importados para los amantes del deporte.</p>
                        </div>
                        <div class="col-11 col-sm-10 col-md-9 col-ls-7 mx-auto boton">
                            <a class="btn-tostore toContacproductos" data-swiper="4" @if(Request::is('/')) href="multimarca" @else href="{{url('/')}}" @endif >@lang('delsay.tostore')</a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div>