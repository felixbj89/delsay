<div id="popularesswiper" class="col-12 swiper-container mx-auto">
    <div class="swiper-wrapper d-lg-flex flex-lg-column">
        <!-- Page 1_1 -->
        <div class="swiper-slide bg-white">
            <div class="row h-100 no-gutters">
                <div class="col-12 col-sm-6 border-r">
                    <div class="row no-gutters h-100 no-gutters popularrowstandar"><!-- Popular 1 -->
                        <div class="col-12 backgroundimg lazyload"
                        data-original="{{asset('img/populares/Populares1_1kimbow2.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                            <a class="btn-tostorelittle d-lg-none" href="https://tienda.mercadolibre.com.ve/kimbow" target="_blank"></a><!-- toContac" data-swiper="5"-->
                        </div>
                        <div class="col-12 d-none d-lg-block">
                            <div class="row no-gutters h-100">
                                <div class="col-12  text-center align-self-center">
                                    <a class="btn-tostore" href="https://tienda.mercadolibre.com.ve/kimbow" target="_blank">@lang('delsay.tostore')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="row no-gutters h-100 no-gutters popularrowstandar"><!-- Popular 2 -->
                        <div class="col-12 backgroundimg lazyload"
                        data-original="{{asset('img/populares/Populares1_2_2kimbow2.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                            <a class="btn-tostorelittle d-lg-none" href="https://tienda.mercadolibre.com.ve/kimbow" target="_blank"></a>
                        </div>
                        <div class="col-12 d-none d-lg-block">
                            <div class="row no-gutters h-100">
                                <div class="col-12  text-center align-self-center">
                                    <a class="btn-tostore" href="https://tienda.mercadolibre.com.ve/kimbow" target="_blank">@lang('delsay.tostore')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide bg-white">
            <div class="row no-gutters h-100 popularrowstandarIII"><!-- Popular 3 -->
                <div class="col-12 backgroundimg lazyload"
                data-original="{{asset('img/populares/Populares3_2converse.jpg')}}" 
                style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                            <a class="btn-tostorelittle d-lg-none toContac" data-swiper="5" @if(Request::is('/')) href="#contacto" @else href="{{url('/')}}" @endif ></a>
                        </div>
                <div class="col-12">
                    <div class="row no-gutters h-100">
                        <div class="col-12  text-center align-self-start">
                            <div class="row no-gutters h-100">
                                <div class="d-flex flex-column align-items-center justify-content-center w-100 px-4">
                                    <h4 class="align-self-center w-100 mb-0"><strong>¡Llévate estos Converse súper originales, pocos modelos!</strong></h4>
                                    <!--p class="align-self-center w-100 mb-0">que eres como todo lo Converse.</p-->
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center align-self-end d-none d-lg-block">
                            <div class="row no-gutters h-100">
                                <div class="col-12  text-center align-self-center">
                                    <a class="btn-tostore toContac" data-swiper="5" @if(Request::is('/')) href="#contacto" @else href="{{url('/')}}" @endif >@lang('delsay.tostore')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page 1_2 -->
        <div class="swiper-slide bg-white">
            <div class="row no-gutters h-100 popularrowstandarIV">
                <div class="col-12 col-sm-5"><!-- Popular 4 -->
                    <div class="row no-gutters h-100">
                        <div class="col-12 align-self-center">
                            <div class="row no-gutters">
                                <div class="col-12 align-self-start">
                                    <div class="w-100 h-100 pl-40px">
                                        <h4><strong>Nueva colección kimbow</strong></h4>
                                        <p>con tela atlética y climatizada<br>para mejorar tu rendimiento</p>
                                    </div>
                                </div>
                                <div class="col-12 text-center align-self-end h-62px d-none d-lg-block">
                                    <div class="row no-gutters h-100">
                                        <div class="col-12  text-center align-self-center">
                                            <a class="btn-tostore mt-2" href="https://tienda.mercadolibre.com.ve/kimbow" target="_blank" >@lang('delsay.tostore')</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-7">
                    <div class="row no-gutters h-100">
                        <div class="col-12 backgroundimg lazyload"
                        data-original="{{asset('img/populares/Populares4_1kimbow.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                            <a class="btn-tostorelittle d-lg-none toContac" data-swiper="5" @if(Request::is('/')) href="#contacto" @else href="{{url('/')}}" @endif ></a>
                        </div>
                    </div>
                </div>
            </div>       
        </div>
        <div class="swiper-slide bg-white">
            <div class="row no-gutters h-100 popularrowstandarIV"><!-- Popular 5 -->
                <div class="col-12 col-sm-7">
                    <div class="row no-gutters h-100">
                        <div class="col-12 backgroundimg lazyload"
                        data-original="{{asset('img/populares/Populares5_2converse.jpg')}}" 
                        style="background-image: url({{asset('img/logos/logodelsaycarga.svg')}});">
                            <a class="btn-tostorelittle d-lg-none toContac" data-swiper="5" @if(Request::is('/')) href="#contacto" @else href="{{url('/')}}" @endif ></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-5">
                    <div class="row no-gutters h-100">
                        <div class="col-12 align-self-center">
                            <div class="row no-gutters">
                                <div class="col-12 align-self-start">
                                    <div class="w-100 h-100 px-4">
                                        <h4><strong>Chuck Taylor negros patentes con detalles en animal print</strong></h4>
                                        <p>¡Te encantarán!</p>
                                        <!--p>disfruta de la nueva</br>
                                        colección primaveral</p-->
                                    </div>
                                </div>
                                <div class="col-12 text-center align-self-end h-62px d-none d-lg-block">
                                    <div class="row no-gutters h-100">
                                        <div class="col-12  text-center align-self-center">
                                            <a class="btn-tostore mt-2 toContac" data-swiper="5" @if(Request::is('/')) href="#contacto" @else href="{{url('/')}}" @endif >@lang('delsay.tostore')</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div>