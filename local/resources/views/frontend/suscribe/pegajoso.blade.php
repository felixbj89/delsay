<a id="btnsuscripcion" class="d-block position-fixed" href="#"></a>
<form id="suscripcionpegajoso" class="row no-gutters w-100" style="display:none" novalidate>
    <div class="col-12 h-100 d-flex align-items-center">
        <div class="w-100 px-2 position-relative">
            <h5 class="m-0 w-100 text-center">@lang('delsay.suscribetitle')</h5>
            <p class="mb-1 w-100 text-center">@lang('delsay.suscribesmall')</p>
            <div class="position-relative mx-auto w-75 w-sm-100">
                <input type="email" class="w-100 form-control-sm" id="emailpegajoso" name="emailsuscribe" placeholder="e-mail">
                <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
            </div>
            <!-- ENVIANDO -->
            <div id="sendingpegajoso" class="row no-gutters h-100 w-100">
                <div class="col-12 h-100 self-center alert alert-dismissible" role="alert">
                    <div class="row no-gutters w-100 h-100 px-2">
                        <div class="col-12">
                            <div class="logobanner self-center" style="background-image: url( {{asset('img/logos/logodelsayII.svg')}} );"></div>
                            <button id="dismispegajoso" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button> 
                        </div>
                        <div class="col-12 align-self-center evn">
                            <div class="row">
                                <div class="col-12 fa-2x text-center">
                                    <i class="fas fa-spinner fa-pulse"></i>
                                </div>
                                <div class="col-12 text-center">
                                    ENVIANDO...
                                </div>
                            </div>
                        </div>
                        <div class="col-12 align-self-center mensaje text-center">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button id="close-suscripcionpegajoso" type="button" class="close m-2"></button>
    </div>
</form>