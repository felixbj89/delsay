<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Delsay |	{{e($marca)}}</title>
		<link rel="shortcut icon" type="image/png" href="{{url('img/favicon_delsay.png')}}">
		<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
		<link href="{{ asset('plugins/swiper/css/swiper.min.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/font-awesome-animation/font-awesome-animation.min.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/MDB-Free/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/MDB-Free/css/mdb.min.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/MDB-Free/css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/PluginBootstrapPopover/css/bootstrap-popover-x.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/masterbrand.css') }}" rel="stylesheet">
		@yield('Mycss')
  </head>
  <body class="container-delsay">

		<div id="brandContainer" class="mx-auto" style="height: 100vh;">
			<div id="navbrand">
				<div class="col-11 mx-auto ">
					<a class="d-inline-flex align-items-center" href="{{url('/')}}"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="44.763px" height="51px" viewBox="0 0 44.763 51" enable-background="new 0 0 44.763 51" xml:space="preserve"><g><line fill="none" stroke="#FFFFFF" stroke-width="3" stroke-miterlimit="10" x1="43.56" y1="24.252" x2="1.203" y2="24.252"/><line fill="none" stroke="#FFFFFF" stroke-width="3" stroke-miterlimit="10" x1="33.153" y1="33.117" x2="1.203" y2="33.117"/></g></svg>Inicio</a>
				</div>
			</div>
			<div id="headerbrand">
				@yield('header-page')
			</div>
			<div id="containerbrand">
				@yield('body-page')
			</div>
			<div id="footerbrand">
					@yield('footer-page')
			</div>
		</div>

		<script type="text/javascript" src="{{ asset('plugins/jquery/jquery-3.3.1.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/lazyload/jquery.lazyload.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/swiper/js/swiper.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/fontawesome-free-5.0.13/svg-with-js/js/fontawesome-all.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/MDB-Free/js/popper.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/MDB-Free/js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/MDB-Free/js/mdb.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/tweenmax/TweenMax.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/tweenmax/plugins/ScrollToPlugin.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/scrollmagic/plugins/animation.gsap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/PluginBootstrapPopover/js/bootstrap-popover-x.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/velocity/velocity.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/velocity/velocity.ui.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/delsay.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/marca.js') }}"></script>
		<!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2Rlv7-WVrAUQtNNkf5Cug2cGZDySkvrs&callback=initMap" async defer></script-->
		@yield("Myscript")
  </body>
</html>
