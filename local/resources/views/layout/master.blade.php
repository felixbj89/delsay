<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="csrf-token" content="{{ csrf_token() }}">
  	<title>Delsay |	Home</title>
		<link rel="shortcut icon" type="image/png" href="{{url('img/favicon_delsay.png')}}">
		<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
		<link href="{{ asset('plugins/swiper/css/swiper.min.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/font-awesome-animation/font-awesome-animation.min.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/MDB-Free/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/MDB-Free/css/mdb.min.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/MDB-Free/css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/PluginBootstrapPopover/css/bootstrap-popover-x.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/master.css') }}" rel="stylesheet">
		@yield('Mycss')
  </head>
  <body class="container-delsay">
	<div id="navdelsay" class="d-flex flex-column flex-sm-row justify-content-between">
		<div class="row no-gutters h-100">
			<div class="col-12 col-sm-11 col-md-10 col-lg-9  mx-auto">
				@include('component.nav')
			</div>
		</div>
		<div>
			<div class="d-flex h-100">
				<button type="button" class="align-self-center toggle-button mx-auto" data-dismiss="popover-x">	
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="44.763px" height="51px" viewBox="0 0 44.763 51" enable-background="new 0 0 44.763 51" xml:space="preserve"><g><g><rect x="17.047" y="4.322" fill="#FFFFFF" width="3" height="42.355"/></g><g><rect x="25.912" y="14.729" fill="#FFFFFF" width="3" height="31.949"/></g></g></svg>
					<svg style="width:20px;" class="mx-auto" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve"><g><g id="cross"><g><polygon points="612,36.004 576.521,0.603 306,270.608 35.478,0.603 0,36.004 270.522,306.011 0,575.997 35.478,611.397      306,341.411 576.521,611.397 612,575.997 341.459,306.011" fill="#FFFFFF"/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
				</button>
			</div>
		</div>
	</div>
	
	<div id="pinContainer" style="height: 100vh;">
		@yield('body-page')
	</div>
	@include('frontend.suscribe.pegajoso')

	<script type="text/javascript" src="{{ asset('plugins/jquery/jquery-3.3.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/lazyload/jquery.lazyload.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/swiper/js/swiper.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/fontawesome-free-5.0.13/svg-with-js/js/fontawesome-all.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/MDB-Free/js/popper.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/MDB-Free/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/MDB-Free/js/mdb.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/tweenmax/TweenMax.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/tweenmax/plugins/ScrollToPlugin.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
	<!--script type="text/javascript" src="{{ asset('plugins/scrollmagic/plugins/TimelineMax.min.js') }}"></script-->
	<script type="text/javascript" src="{{ asset('plugins/scrollmagic/plugins/animation.gsap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/PluginBootstrapPopover/js/bootstrap-popover-x.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/velocity/velocity.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/velocity/velocity.ui.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/delsay.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2Rlv7-WVrAUQtNNkf5Cug2cGZDySkvrs&callback=initMap" async defer></script>
	@yield("Myscript")
  </body>
</html>
