<?php

return [
    //SLIDENAV
    'buscar' => 'BUSCAR',
    'navliI' => 'Productos',
    'navliII' => 'Populares',
    'navliIII' => 'Noticias',
    'navliIV' => 'Tiendas',
    'navliV' => 'Acerca de',
    'navliVI' => 'Contacto',
    'navliiI' => 'La Empresa',
    'navliiII' => 'Filosofía',
    'navliiIII' => 'Marcas',
    'navliiiI' => 'Acceder',
    'navliiiII' => 'Registrarse',
    //PRODUCTOS
    'tostore' => 'IR A LA TIENDA',
    //ACERCADE
    'tabI' => 'LA EMPRESA',
    'tabII' => 'FILOSOFÍA',
    'tabIII' => 'MARCAS',
    //FOOTER
    'footerI' => 'DISTRIBUIDORA DEL SAY C.A J-00000000-0',
    'footerII' => 'Hacienda Creativa 2018 Todos los derechos reservados',
    //SUSCRIBE
    'suscribetitle' => "¡SUSCRÍBETE!",
    'suscribesmall' => '¡Entérate de nuestras ofertas y noticias y recibe descuentos especiales!'
];