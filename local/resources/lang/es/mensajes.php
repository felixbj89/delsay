<?php
return [
    //@lang('mensajes.')
    'error_parametros' => 'Parametros Incompletos',
    'error_email' => 'Formato de correo es incorrecto',
    'error_sendmail' => 'No se pudo enviar el correo',
    'sus_sendmail' => 'Gracias por envianos su información',
    'sus_exists' => 'El suscriptor existe'
];