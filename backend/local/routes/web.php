<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"LoginController@index");
Route::group(["prefix"  => "delsay"], function () {
	//GET
	Route::get('login', ['uses' => 'LoginController@index', 'as' => 'delsay/login']);
	Route::get('salir', ['uses' => 'DashboardController@salir', 'as' => 'delsay/salir']);
	Route::get('dashboard/{codigo}', ['uses' => 'DashboardController@dashboard', 'as' => 'delsay/dashboard/{codigousuario}']);
	Route::get('galeria/{codigo}', ['uses' => 'DashboardController@galeria', 'as' => 'delsay/galeria/{codigousuario}']);
	//POST
	Route::post('login', ['uses' => 'LoginController@autentificarse', 'as' => 'delsay/login']);
	Route::post('galeria/{codigousuario}', ['uses' => 'GaleriaController@galeria', 'as' => 'delsay/galeria/{codigousuario}']);
});