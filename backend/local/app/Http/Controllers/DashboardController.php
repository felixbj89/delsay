<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class DashboardController extends Controller
{
    private function procesarGET($route){
		$url = $route;
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch); return $result;
	}
	
	private function procesarPOST($route,$entidad){
		$url = $route;
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// indicamos el tipo de petición: POST
		curl_setopt($ch, CURLOPT_POST, TRUE);
		//le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $entidad);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch); return $result;
	}

	//DASHBOARD
	public function dashboard($codigo){
		return view("backend.dashboard",[
			"codigo" => $codigo
		]);
	}
	
	//STORE
	public function galeria($codigo){
		return view("backend.galeria",[
			"codigo" => $codigo
		]);
	}
	
	//SALIR
	public function salir(){
		Session::flush();
		return redirect()->to("/");
	}
}
