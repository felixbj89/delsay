<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<title>delsay | Inicio</title>
			<!-- Tell the browser to be responsive to screen width -->
			<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
			<!-- Bootstrap 3.3.7 -->
			<link rel="stylesheet" href="{{asset('plugins/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
			<!-- Font Awesome -->
			<link rel="stylesheet" href="{{asset('plugins/bower_components/font-awesome/css/font-awesome.min.css')}}">
			<!-- Ionicons -->
			<link rel="stylesheet" href="{{asset('plugins/bower_components/Ionicons/css/ionicons.min.css')}}">
			<!-- Theme style -->
			<link rel="stylesheet" href="{{asset('plugins/dist/css/AdminLTE.min.css')}}">
			<!-- Material Design -->
			<link rel="stylesheet" href="{{asset('plugins/dist/css/skins/all-md-skins.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/media/css/dataTables.bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.dataTables.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/css/buttons.bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/extensions/Responsive/css/responsive.bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/dist/css/bootstrap-material-design.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/dist/css/ripples.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/dist/css/MaterialAdminLTE.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/summernote-0.8.9-dist/dist/summernote.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/lightbox2-master/dist/css/lightbox.min.css')}}">
			<!-- Bootstrap Color Picker -->
			<link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
			@yield("mi-css")
			<link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
			<link rel="stylesheet" href="{{asset('css/general.css')}}">
			<link rel="stylesheet" href="{{asset('css/container.css')}}">
			<link rel="stylesheet" href="{{asset('css/sidebar.css')}}">
			<link rel="stylesheet" href="{{asset('css/modales.css')}}">
			<!-- AdminLTE Skins. Choose a skin from the css/skins
			folder instead of downloading all of them to reduce the load. -->
			<link rel="stylesheet" href="{{asset('plugins/dist/css/skins/all-md-skins.min.css')}}">
			<link rel="shortcut icon" type="image/png" href="{{url('img/favicon_delsay.png')}}">
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<!-- Google Font -->
			<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		</head>
		<body id="top" class="skin-blue fixed sidebar-mini sidebar-mini-expand-feature" style="height: auto; min-height: 100%;padding-rigt:0px !important">
			<!-- Site wrapper -->
			<div class="wrapper">
				<header class="main-header">
					<!-- Logo -->
					<a href="{{url('ushop/dashboard')}}" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
						<span class="logo-mini"><img src="{{asset('img/logo/logodelsay.svg')}}" width="32" height="32"></span>
						<!-- logo for regular state and mobile devices -->
						<span class="logo-lg"><img src="{{asset('img/logo/logodelsay.svg')}}" width="110" height="64"></span>
					</a>
					<!-- Header Navbar: style can be found in header.less -->
					<nav class="navbar navbar-static-top">
						<!-- Sidebar toggle button-->
						<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">
								<!-- Messages: style can be found in dropdown.less-->
								<!-- Control Sidebar Toggle Button -->
								<li>					
									<a id="clicksalirheader" href="#" data-toggle="control-sidebar"><i class="fa fa-sign-out"></i></a>
								</li>
								<li>					
									<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
								</li>
							</ul>
						</div>
					</nav>
				</header>
				<aside class="main-sidebar">
					<!-- sidebar: style can be found in sidebar.less -->
					<section class="sidebar">
						<!-- search form -->
						<form action="#" method="get" class="sidebar-form">
							<div class="input-group">
								<input type="text" name="q" class="form-control" placeholder="Buscar...">
								<span class="input-group-btn">
									<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</form>
						<!-- /.search form -->
						<!-- sidebar menu: : style can be found in sidebar.less -->
						<ul class="sidebar-menu" data-widget="tree">
							<li class="header">MENU PRINCIPAL</li>
							<li class="li-style @yield('dashboardmodulo')">
								<a href="{{url('delsay/dashboard/'.$codigo)}}">
									<i class="fa fa-dashboard"></i> <span>Inicio</span>
								</a>
							</li>
							<li class="li-style @yield('mediamodulo')">
								<a href="{{url('delsay/galeria/'.$codigo)}}">
									<i class="fa fa-file-picture-o"></i> <span>Galerías</span>
								</a>
							</li>
							<li class="header">CERRAR SESIÓN</li>
							<li class="li-style">
								<a id="clicksalir" href="#">
									<i class="fa fa-sign-out"></i> <span>Salir</span>
								</a>
							</li>
						</ul>
					</section>
					<!-- /.sidebar -->
				</aside>
				<!-- =============================================== -->
				<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
					<!-- Content Header (Page header) -->
					<section class="content-header">
						<h1>
							@yield("section-title")
						</h1>
						<ol class="breadcrumb">
							@yield("section-breadcrumb")
						</ol>
					</section>
					<!-- Main content -->
					<section class="content">
						<!-- Default box -->
						@yield("section-body")	
					</section>
					<!-- /.content -->
				</div>
				<!-- /.content-wrapper -->
				<footer class="main-footer">
					<div class="pull-right hidden-xs">
						<b>Version</b> 0.0.2
					</div>
					<strong>Copyright &copy; 2018 <a href="#">Delsay</a>
					All rights reserved, 
					Powered By
					<a href="https://www.haciendacreativa.net">Hacienda C.A.</a>.</strong>
				</footer>
				<!-- /.control-sidebar -->
				<!-- Add the sidebar's background. This div must be placed
				immediately after the control sidebar -->
			</div>
			<!-- ./wrapper -->
			<!-- jQuery 3 -->
			<script src="{{asset('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
			<!-- Bootstrap 3.3.7 -->
			<script src="{{asset('plugins/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
			<!-- OTROS -->
			<script src="{{asset('plugins/lightbox2-master/dist/js/lightbox.min.js')}}"></script>
			<!-- Material Design -->
			<script src="{{asset('plugins/DataTables-1.10.13/media/js/jquery.dataTables.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/media/js/dataTables.bootstrap.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.bootstrap.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.print.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
			<script src="{{asset('plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
			<script src="{{asset('plugins/dist/js/material.min.js')}}"></script>
			<script src="{{asset('plugins/dist/js/ripples.min.js')}}"></script>
			<script src="{{asset('plugins/summernote-0.8.9-dist/dist/summernote.js')}}"></script>
			<script src="{{asset('plugins/summernote-0.8.9-dist/dist/es-ES.js')}}"></script>
			<script src="{{asset('js/validaciones.js')}}"></script>
			<script src="{{asset('js/wizard.js')}}"></script>
			<script src="{{asset('js/tabla.js')}}"></script>
			<script src="{{asset('js/controlador/callajax/dashboard.js')}}"></script>
			<script src="{{asset('js/controlador/dashboard.js')}}"></script>
			@yield("mi-scripts")
			<script>
				$.material.init();
			</script>
			<!-- SlimScroll -->
			<script src="{{asset('plugins/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
			<!-- FastClick -->
			<script src="{{asset('plugins/bower_components/fastclick/lib/fastclick.js')}}"></script>
			<!-- AdminLTE App -->
			<script src="{{asset('plugins/dist/js/adminlte.min.js')}}"></script>
			<!-- AdminLTE for demo purposes -->
			<script src="{{asset('plugins/dist/js/demo.js')}}"></script>
			<script>
				$(document).ready(function () {
					$('.sidebar-menu').tree()
				})
			</script>
		</body>
	</html>
