@extends("layout.dashboard")
@section("mediamodulo")
	active
@endsection
@section("section-title")
	Galerías.
@endsection
@section("section-breadcrumb")
	<li>
		<a href=""><i class="fa fa-dashboard"></i> Inicio</a>
	</li>
	<li class="active">
		<a href=""><i class="fa fa-file-picture-o"></i> Galerías</a>
	</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv2.css')}}">
@endsection
@section("section-body")
	<div class="box">
		<div class="box-body">
			<hr/>
			<div class="row">
				<div class="col-xs-12 container-box">
					<div class="wizard container-wizard">
						<div class="wizard-inner container-wizard-body">
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active section mostrar">
									<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Secciones & Tipo fuentes">
										<span class="round-tab">
											<i class="fa fa-pencil"></i>
										</span>
									</a>
								</li>
								<li role="presentation" class="disabled imagenes ocultar">
									<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Fuentes">
										<span class="round-tab">
											<i class="fa fa-file-picture-o"></i>
										</span>
									</a>
								</li>
							</ul>
							<form id="form-galeria" class="form-horizontal" action="{{url('delsay/galeria/'.$codigo)}}" method="post" enctype="multipart/form-data">
								<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
								<div class="tab-content">
									<div class="tab-pane active" role="tabpanel" id="step1">
										<div class="row">
											<div class="col-xs-12 col-md-12 is-empty">
												<div class="box">
													<div class="box-header with-border">
														<h3 class="box-title">GESTIONE SU TEXTO</h3>
													</div>
												</div>
											</div>
										</div>
										<hr/>
										<div class="row">
											<div class="col-xs-12 col-md-12 is-empty">
												<label class="control-label clearfix"><span class="text-delsay">Descripción</span></label>
												<div id="descripcion"></div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
												<ul class="list-inline pull-right">
													<li><button type="button" class="btn btn-primary next-step goimagenes">CONTINUAR</button></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="tab-pane" role="tabpanel" id="step2">
										<div class="row">
											<div class="col-xs-12 col-md-12 is-empty">
												<div class="box">
													<div class="box-header with-border">
														<h3 class="box-title">GESTIONE SU IMAGEN #RESOLUCIÓN: 1920x1080PX</h3>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
												<label class="control-label clearfix"><span class="text-delsay">Texto Alt</span></label>
												<div class="input-group">
													<span class="input-group-addon icon-right"><i class="fa fa-pencil"></i></span>
													<div class="form-group is-empty">
														<input class="form-control" placeholder="Ingrese su texto alternativo" type="text" id="textoalt" name="textoalt">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-md-12 is-empty">
												<label class="control-label clearfix"><span class="text-delsay">Imagen de su agrado</span></label>
												<div class="container-picture">
													<a id="logo-a" class="logo-link" href="{{url('/').'/img/logo/logodelsay.svg'}}" data-lightbox="logo" class="spacing-picture">
														<img id="logo-img" src="{{url('/').'/img/logo/logodelsay.svg'}}" class="logo img-responsive img-thumbnail"/>
													</a>
													<div>
														<label for="imagenpicture" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
														<input type="file" id="imagenpicture" name="imagenpicture" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile(event)"  fileread="banner.imagen"/>
														<p class="help-block">Solo es permitido imágenes jpg/png.</p>
													</div>
												</div>
											</div>
										</div>
										<hr/>
										<div class="row">
											<div class="col-xs-12 col-md-12 is-empty">
												<div class="box-body">
													<a id="btnGuardar" class="btn btn-app btn-delsay">
														<i class="fa fa-save"></i> GUARDAR
														<div class="ripple-container"></div>
													</a>
													<a class="btn btn-app btn-delsay">
														<i class="fa fa-list"></i> ADMINISTRAR
														<div class="ripple-container"></div>
													</a>
													<a class="btn btn-app btn-delsay">
														<i class="fa fa-dashboard"></i> INICIO
														<div class="ripple-container"></div>
													</a>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-md-12 is-empty">
												<ul class="list-inline pull-right">
													<li><button type="button" class="btn btn-default prev-step goseccion">ATRAS</button></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<hr/>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logodelsay.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logodelsay.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 flex-center">
							<p id="waitadvertencia" class="text-center font-bold"><strong>Espere un momento.</strong></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x container-wait"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
@endsection
@section("mi-scripts")
	<script src="{{asset('js/novo/formularios.js')}}"></script>
	<script src="{{asset('js/novo/validaciones.js')}}"></script>
	<script src="{{asset('js/novo/modales.js')}}"></script>
	<script src="{{asset('js/controlador/galeria.js')}}"></script>
@endsection