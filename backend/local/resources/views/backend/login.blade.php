<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<title>Delsay | Log in</title>
			<!-- Tell the browser to be responsive to screen width -->
			<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
			<!-- Bootstrap 3.3.7 -->
			<link rel="stylesheet" href="{{asset('plugins/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
			<!-- Font Awesome -->
			<link rel="stylesheet" href="{{asset('plugins/bower_components/font-awesome/css/font-awesome.min.css')}}">
			<!-- Theme style -->
			<link rel="stylesheet" href="{{asset('plugins/dist/css/AdminLTE.min.css')}}">
			<!-- Material Design -->
			<link rel="stylesheet" href="{{asset('plugins/dist/css/bootstrap-material-design.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/dist/css/ripples.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/dist/css/MaterialAdminLTE.min.css')}}">
			<link rel="shortcut icon" type="img/png" href="{{asset('img/logo/fav.png')}}">
			<!-- iCheck -->
			<!-- <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css"> -->
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<link rel="stylesheet" href="{{asset('css/login.css')}}">
			<link rel="shortcut icon" type="image/png" href="{{url('img/favicon_delsay.png')}}">
			<!-- Google Font -->
			<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		</head>
		<body class="hold-transition login-page">
			<div class="login-box item-grid-login">
				<!-- /.login-logo -->
				<div class="login-box-body item-border-login">
					<p class="login-box-msg"><img src="{{asset('img/logo/logodelsay.svg')}}" width="128"></p>
					<hr/>
					<form action="../../index2.html" method="post">
						<div class="row">
							<div class="col-xs-12">
								<div class="box item-box">
									<div class="box-header">
										<label>Ingrese sus credenciales para continuar.</label>
									</div>
								</div>
							</div>
						</div>
						<div class="box item-box">
							<div class="box-body item-box">
								<div class="row">
									<!-- /.col -->
									<div class="col-xs-12">
										<div class="btn-group">
										  <button type="button" class="btn btn-default"><i class="fa fa-check"></i> Recordar</button>
										  <button type="button" class="btn btn-default"><i class="fa fa-undo"></i> Regresar</button>
										</div>
									</div>
									<!-- /.col -->
								</div>
								<div class="form-group has-feedback item-form-group">
									<input type="email" id="email" name="email" class="form-control" placeholder="Email" value="{{old('email')}}">
									<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
								</div>
								<div class="form-group has-feedback item-form-group">
									<input type="password" class="form-control" id="password" name="password" placeholder="Password" value="{{old('password')}}">
									<span class="glyphicon glyphicon-lock form-control-feedback"></span>
								</div>
								<div class="row">
									<!-- /.col -->
									<div class="col-xs-12">
										<button type="button" id="btnAcceder" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
											<i class="fa fa-sign-in" aria-hidden="true"></i> ACCEDER
											<div class="ripple-container"></div>
										</button>
									</div>
									<!-- /.col -->
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- /.login-box-body -->
			</div>
			<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					<div class="modal-content item-modal-border">
						<div class="modal-header item-modal-header">
							<img src="{{asset('img/logo/logodelsay.svg')}}" width="128">
						</div>
						<div class="modal-body">
							<p id="messageadvertencia" class="text-center delsay-fonts"></p>
						</div>
						<div class="modal-footer item-modal-footer">
							<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
								<i class="fa fa-times" aria-hidden="true"></i> CERRAR
								<div class="ripple-container"></div>
							</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					<div class="modal-content item-modal-border">
						<div class="modal-header item-modal-header">
							<img src="{{asset('img/logo/logodelsay.svg')}}" width="128">
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<p id="waitadvertencia" class="text-center font-bold"><strong>Espere un momento.</strong></p>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 item-grid-wait container-wait">
									<i class="fa fa-spinner fa-spin fa-2x container-wait"></i>
								</div>
							</div>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- jQuery 3 -->
			<script src="{{asset('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
			<!-- Bootstrap 3.3.7 -->
			<script src="{{asset('plugins/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
			<!-- Material Design -->
			<script src="{{asset('plugins/dist/js/material.min.js')}}"></script>
			<script src="{{asset('plugins/dist/js/ripples.min.js')}}"></script>
			<script src="{{asset('js/validaciones.js')}}"></script>
			<script src="{{asset('js/controlador/servicios/login.js')}}"></script>
			<script src="{{asset('js/controlador/login.js')}}"></script>
			<script>
				$.material.init();
			</script>
		</body>
	</html>