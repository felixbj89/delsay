var NovoForms = {
	createEditor:function(id,textplaceholder){
		//CREO UN SUMMERNOTE
		$("#" + id).summernote({
			lang: 'es-ES',
			toolbar: [
				['style', ['bold', 'italic', 'underline', 'clear']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
			],
			fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '32'],
			height: 300,
			minHeight: null,
			maxHeight: null,
			callbacks: {
				onChange:function(event){},
				onFocus: function() {},
				onKeyup: function(e) {},
				onPaste: function(e){
					//e.originalEvent.clipboardData.getData('text')
					var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
					e.preventDefault();
					setTimeout(function () {
						document.execCommand('insertText', false, bufferText);                    
					}, 10);						
				}
			},
			placeholder:textplaceholder
		});
	},
	uploadImage:function(id,ancho,alto,mb){
		var _URL = window.URL || window.webkitURL;
		var seleccion = undefined;
		$(id).bind("change",function(){
			if ((file = this.files[0])) {
				img = new Image();
				img.onload = function(){
					if((img.width != ancho && img.height !=alto) || (img.width == ancho && img.height != alto)){
						var url = window.location.href.substr(0,window.location.href.search("backend"));
						$("#logo-a").attr("href",(url + "backend/img/logo/logodelsay.svg"));
						$("#logo-img").attr("src",(url + "backend/img/logo/logodelsay.svg"));
							
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Las medidas de su imagen no cumple con lo establecido.</strong>");
						$("#modal_alerta").modal("show");
					}
				}
				
				img.src = _URL.createObjectURL(file);
				if((file.size/1024) > mb){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>Su imagen no pueden exceder los 3MB.</strong>");
					$("#modal_alerta").modal("show");
				}else{
					if(file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Formato incorrecto, Intente nuevamente.</strong>");
						$("#modal_alerta").modal("show");
					}else{
						$("#logo-a").attr("href",img.src);
						$("#logo-img").attr("src",img.src);
					}
				}
			}
		});
	},
	stripHTML:function(html){
		// Create a new div element
		var temporalDivElement = document.createElement("div");
		// Set the HTML content with the providen
		temporalDivElement.innerHTML = html;
		// Retrieve the text property of the element (cross-browser support)
		return temporalDivElement.textContent || temporalDivElement.innerText || "";
	}
};