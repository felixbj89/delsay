var NovoValidations = {
	isEmpty:function(id){
		if($(id).val().length==0){
			return 1;
		}else{
			return 0;
		}
	},
	isEmptyString:function(string){
		if(string.length == 0){
			return 1;
		}else{
			return 0;
		}
	},
	isFocusEmpty:function(id){
		$(id).bind("focusout",function(){
			console.log(NovoValidations.isEmpty(id));
		});
	},
};