function goURL(route){
	return window.location.href.substr(0,window.location.href.search("delsay")) + route;
}

$(document).ready(function(){
	$('.sidebar-menu').tree()
	
	lightbox.option({
		'resizeDuration': 200,
		'wrapAround': true,
		'albumLabel':''
	});
	
	if(localStorage.getItem("delsay")!=undefined){
		var usuario = JSON.parse(atob(localStorage.getItem("delsay")));
		$("#nombresidebarusuario").text(usuario.users_name);
		$(document).on("click","#clicksalir",function(){
			delete localStorage.delsay;
			delete localStorage.codigousuario;
			window.location.href = goURL("delsay/salir");
		});
		
		$(document).on("click","#clicksalirheader",function(){
			delete localStorage.delsay;
			delete localStorage.codigousuario;
			window.location.href = goURL("delsay/salir");
		});
	}
});