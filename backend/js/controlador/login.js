$(document).ready(function(){
	let alerta = "";
	
	$("#btnAcceder, #email, #password").on("keypress",function(event){
		if(event.which==13){
			if(checkLength($("#email").val()) && checkLength($("#password").val())){
				$("#messageadvertencia").text("Todos los campos son obligatorios.");
				$("#modal_alerta").modal("show");
			}else if(checkLength($("#email").val())){
				$("#messageadvertencia").text("El E-mail ha ingresar es obligatorio.");
				$("#modal_alerta").modal("show");
			}else if(!checkEmail($("#email").val())){
				$("#messageadvertencia").text("El Formato de su e-mail ha ingresar es obligatorio.");
				$("#modal_alerta").modal("show");
			}else if(checkLength($("#password").val())){
				$("#messageadvertencia").text("El password ha ingresar es obligatorio.");
				$("#modal_alerta").modal("show");
			}else{
				formData = new FormData();
				formData.append("correo",$("#email").val());
				formData.append("password",$("#password").val());
				acceder("delsay/login",formData);
			}
		}
	});
	
	$("#btnAcceder").on("click",function(){
		if(checkLength($("#email").val()) && checkLength($("#password").val())){
			$("#messageadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if(checkLength($("#email").val())){
			$("#messageadvertencia").text("El E-mail ha ingresar es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if(!checkEmail($("#email").val())){
			$("#messageadvertencia").text("El Formato de su e-mail ha ingresar es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if(checkLength($("#password").val())){
			$("#messageadvertencia").text("El password ha ingresar es obligatorio.");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("correo",$("#email").val());
			formData.append("password",$("#password").val());
			acceder("delsay/login",formData);
		}
	})
});