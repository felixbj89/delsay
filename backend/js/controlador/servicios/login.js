function acceder(route,dataforms){
	var url = window.location.href;
	console.log(url + route);
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){			
				delete localStorage.delsay;
				delete localStorage.codigousuario;
				let usuario = JSON.parse(respuesta.usuario);
				localStorage.setItem("delsay",btoa(respuesta.usuario));
				localStorage.setItem("codigousuario",usuario.users_codigo);
				window.location.href = (url + "delsay/dashboard/" + usuario.users_codigo);
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}