var pic = undefined;

function openFile(event){
  pic = event;
}

$(document).ready(function(){
	$("#textoalt").focus();
	
	//CREO UN EDITOR;
	NovoForms.createEditor("descripcion","Ingrese la descripción de su agrado");
	
	//SETUP-TEXTOALT
	$("#textoalt").val("");
	NovoValidations.isFocusEmpty("#textoalt");
	
	//SETUP-PICTURE
	NovoForms.uploadImage("#imagenpicture",1920,1080,30000);
	
	//SETUP-GUARDAR
	$(document).on("click","#btnGuardar",function(){
		if(NovoValidations.isEmpty("#textoalt")){
			NovoModales.showMessageAlerta("El texto alternativo es obligatorio.");
		}else if(pic==undefined){
			NovoModales.showMessageAlerta("La imagen es obligatoria.");
		}else{
			NovoModales.waitModal();
			$("#form-galeria").submit();
		}
	});
});