<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('users_nombre');
			$table->string('users_codigo');
            $table->string('users_email')->unique();
            $table->string('users_password');
			$table->enum('users_role',['0','1']);
            $table->rememberToken();
            $table->timestamps();
        });
		
		User::create([
			"users_nombre" => "Hacienda Creativa C.A.",
			"users_codigo" => str_random(20),
			"users_email" => "haciendacreativa@gmail.com",
			"users_password" => hash("md5","hacienda"),
			"users_role" => "0"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
