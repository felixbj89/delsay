<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "usuarios";
	
    protected $fillable = [
        'users_nombre',
		'users_codigo',
		'users_email',
		'users_password',
		'users_role'
    ];

    protected $hidden = [
        'id',
    ];
	
	public static function autentificarse($inputs){
		$usuario = User::where("users_email",$inputs["correo"])->where("users_password",hash("md5",$inputs["password"]));
		if($usuario->first()!=NULL){
			return [$usuario->first()->toJson(),"User@functionautentificarse",200];
		}else{
			return [NULL,"User@functionautentificarse",500];
		}
	}
}
