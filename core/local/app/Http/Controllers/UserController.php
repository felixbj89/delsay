<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function autentificarse(Request $request){
		$estado = User::autentificarse($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"call-function" => $estado[1],
			"code" => $estado[2],
		],$estado[2]);
	}
}
